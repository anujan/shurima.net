$(document).ready(function() {
    var beat = false;
    var lockedIn = false;
    var cards = new Array("http://shurima.net/images/BlueCard.jpg", "http://shurima.net/images/RedCard.jpg", "http://shurima.net/images/GoldCard.jpg");
    var cardNames = new Array("Blue", "Red", "Gold");
    var currentCard = Math.floor(Math.random()*3);
    var inactive = "http://shurima.net/images/PickaCard.jpg";
    var timer;
    var lockedInCard = -1;
    var cooldown = 0;
    var cdTimer;
    var lesson = false;
    window.pulse_image = $("#card");
    $(this).keydown(function(e) {
        lesson = $("#lessonBox").prop("checked");
        if (e.keyCode == 87) {
            if (cooldown > 0)
                return false;
            if (!beat) {
                $("#card").attr("src", cards[currentCard]);
                $("#result").html("<strong>Pick a card...</strong>");
                timer = setInterval(function() {
                    if (beat) {
                        currentCard++;
                        if (currentCard > 2)
                            currentCard = 0;
                        if (lockedIn) {
                            PulseAnimation();
                        } else {
                            $("#card").attr("src", cards[currentCard]);
                        }
                        if (lesson) {
                            $("#lesson").html(cardNames[currentCard]);
                        }
                    }
                }, 515);
                beat = true;
            } else if (!lockedIn) {
                lockedIn = true;
                lockedInCard = currentCard;
                $("#result").html("<strong>" + cardNames[lockedInCard] + " Card locked!</strong>");
            } else {
                return false; //disable w for throw
            }
        }
        $(document).bind("contextmenu",function(e) {
            if (lockedIn) {
                clearInterval(timer);
                beat = false;
                lockedIn = false;
                $("#result").html("<strong>" + cardNames[lockedInCard] + " Card thrown</strong>");
                $("#card").attr("src", inactive);
                cooldown = 6;
                $("#cooldown").html("Cooldown: " + cooldown + " seconds");
                cdTimer = setInterval(function() {
                    cooldown--;
                    if (cooldown < 1) {
                        clearInterval(cdTimer);
                        $("#cooldown").html("The skill is off cooldown now!");
                    }
                    $("#cooldown").html("Cooldown: " + cooldown + " seconds");
                }, 1000);
            }
            return false;
        });
    });
});
function PulseAnimation()
{
    var minOpacity = .33;
    var fadeOutDuration = 515/2;
    var fadeInDuration = 515/2;
	
    window.pulse_image.animate({
        opacity: minOpacity
    }, fadeOutDuration, function() {
        window.pulse_image.animate({
            opacity: 1
        }, fadeInDuration)
    });
}