/*
 * jsound 1.0.1
 *
 * JSound, a jQuery plugin for playing sounds on any html tag element
 *
 * Copyright (c) 2012 Ayman Teryaki
 * Havalite CMS (http://havalite.com/)
 * MIT style license, FREE to use, alter, copy, sell, and especially ENHANCE
 *
 *
 */
var soundElem = 'a, p, input';
var allElems = false;
var aHover = 'beep';
var aLeave = 'select';
var aClick = 'page';
var aFocusIn = 'page';
var aFocusOut = 'select';

function playSound(file, loop){
	var mp3File = file + ".mp3";
        stopSounds();
	return '<audio autoplay="autoplay"><source src="' + mp3File + '" type="audio/mpeg"></audio>'
}

function stopSounds(){ $('audio').remove(); }

$(document).ready( function(){
	
	var apCode = '<div id="audioContainer" style="position:absolute; top:-200px;">'+
		'<span id="hoverSound"></span>'+
		'<span id="clickSound"></span>'+
		'<span id="leaveSound"></span>'+
		'<span id="moveSound"></span>'+
		'<span id="focusInSound"></span>'+
		'<span id="focusOutSound"></span>'+
		'<span id="startSound"></span>'+
		'</div>';
	
	$('body').append(apCode);

	$(soundElem).each(function(){
		if(allElems){
			if(aHover || aLeave){
				$(this).hover(
					function(){ $('#hoverSound').html( playSound(aHover)); },
					function(){  $('#leaveSound').html( playSound(aLeave)); }
				);
			}
			if(aClick){
				$(this).click(function(){ $('#clickSound').html( playSound(aClick)); });
			}
			
			if(aFocusIn || aFocusOut){
				$(this).focusin(function(){ $('#focusInSound').html( playSound(aFocusIn)); });
				$(this).focusout(function(){ $('#focusOutSound').html( playSound(aFocusOut)); });
			}
		}
		else{
			if($(this).attr('hoverSound') || $(this).attr('leaveSound')){
				var audioFile1 = $(this).attr('hoverSound');
				var audioFile2 = $(this).attr('leaveSound');
				$(this).hover(
					function(){ $('#hoverSound').html( playSound(audioFile1)); },
					function(){ $('#leaveSound').html( playSound(audioFile2)); }
				);
			}
			
			if($(this).attr('clickSound')){
				var audioFile = $(this).attr('clickSound');
				$(this).mousedown(function(){ $('#clickSound').html( playSound(audioFile)); });
			}
			
			if($(this).attr('moveSound')){
				var audioFile = $(this).attr('moveSound');
				$(this).mousemove(function(){ $('#moveSound').html( playSound(audioFile)); });
			}
			
			if($(this).attr('focusInSound') || $(this).attr('focusOutSound')){
				var audioFile1 = $(this).attr('focusInSound');
				var audioFile2 = $(this).attr('focusOutSound');
				$(this).focusin(function(){ $('#focusInSound').html( playSound(audioFile1)); });
				$(this).focusout(function(){ $('#focusOutSound').html( playSound(audioFile2)); });
			}
			
			if($(this).attr('startSound')){
				var audioFile = $(this).attr('startSound');
				var loopit = false;
				if($(this).attr('loopSound')) loopit = true;
				$(this).ready(function(){ 
					$('#startSound').html( playSound(audioFile, loopit)); 
				});
			}
		}
	});	
});
