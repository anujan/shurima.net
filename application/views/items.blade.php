@layout('master')
@section('title')
Items
@endsection
@section('description')
ShurimaDB is a information database of all items and champions in League of Legends. Every item in the game along with stats, prices, and build paths are shown and updated regularly!
@endsection
@section('assets')
@parent
<style>
#gallery-list img {
border-radius: 12px 12px 12px 12px;
box-shadow: 0 0 3px #000000;
height: 62px !important;
width: 62px !important;
}
</style>
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">

        <div class="span12" id="content">

            <div id="post" class="post-single">
                <div class="content-outer">
                    <div class="content-inner">
                        <div id="gallery">
                            <ul class="gallery-navigation">
                                <li><a class="btn btn-primary active" id="all" href="#">All</a></li>
                                @for ($i = 65; $i < 91; $i++)
                                <li><a href="#" class="btn filter" id="cat-{{$i}}" title="View all Items filed under {{chr($i)}}">{{chr($i)}}</a></li>
                                @endfor
                            </ul>
                            <div class="separator"></div>
                            <div class="row-fluid" id="gallery-list">
                                <?php $count = 0; ?>
                                <ul>
                                    {{Utilities::getItemList()}}
                                </ul>
                            </div>
                        </div>

                    </div>

                    <!-- content inner -->
                </div>
                <!-- content outer -->
            </div>

        </div>


    </section>
    <!-- End Main Content -->

</div>
@endsection