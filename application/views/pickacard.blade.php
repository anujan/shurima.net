@layout('master')

@section('title')
Twisted Fate's Pick a Card
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->
    <section class="row" id="main-content">
        <div class="span12" id="content">
            <div id="post" class="post-single">
                <div class="content-outer">
                    <div class="content-inner">
                        {{Utilities::adsense()}}
                        <h2 id="result" style="text-align: center">
                        </h2>

                        <h3 id="cooldown"></h3>
                        <img id="card" class="pulse_image" width="45%" height="45%"
                             src="http://images1.wikia.nocookie.net/__cb20120820060002/leagueoflegends/images/d/da/PickaCard.jpg">

                        <h3 id="lesson"></h3>
                        <label for="lessonBox">Lesson Mode?</label>
                        <input type="checkbox" id="lessonBox"/>
                    </div>
                    <aside class="sidebar right-sidebar">
                        <div class="sidebar-widget">
                            <h3>Instructions</h3>
                            <ul class="custom-menu">
                                <li>Press W to start rotating cards</li>
                                <li>Press W again to lock in a card</li>
                                <li>Right click to throw the card</li>
                                <li>The first time may be buggy depending on how fast you load the images, but it'll be
                                    fixed everytime onwards
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!-- End Main Content -->
</div>
<script src="{{asset('js/pickacard.js')}}"></script>
@endsection