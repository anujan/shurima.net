@layout('master')

@section('title')
{{$champion->displayname}}
@endsection
@section('description')
{{$champion->description}}
@endsection
@section('assets')
@parent
<script type="text/javascript" src="{{asset('js/plugins/load-image.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap/bootstrap-image-gallery.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/plugins/bootstrap-image-gallery.css')}}">
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span10" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{route('champions')}}">Champions <span class="divider">&raquo;</span></a></li>
                <li class="typ-pin"><a href="{{route('champions')}}">{{$champion->displayname}}</a></li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        <div class="tab-header">
                            <ul id="tab-content" class="nav nav-tabs">
                                <li class="active"><a href="#tab1">Statistics</a></li>
                                <li><a href="#tab2">Abilities</a></li>
                                <li><a href="#tab3">Background</a></li>
                                <li><a href="#tab4">Skins</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane active">
                                <div class="review-block span3">
                                    <div class="thumbnail" style="height: 84px; width: 84px;">
                                        <img alt="{{$champion->displayname}}" src="{{asset(Utilities::$champion_assets_path . $champion->iconpath)}}">
                                    </div>
                                    <div class="criteria">
                                        <span class="name">Attack</span>
                                        <span class="value">{{($champion->ratingattack * 10)}}</span>
                                        <div class="progress">
                                            <div style="width: {{($champion->ratingattack * 10)}}%;" class="bar"></div>
                                        </div>
                                    </div>
                                    <div class="criteria">
                                        <span class="name">Magic</span>
                                        <span class="value">{{($champion->ratingmagic * 10)}}</span>
                                        <div class="progress">
                                            <div style="width: {{($champion->ratingmagic * 10)}}%;" class="bar"></div>
                                        </div>
                                    </div>
                                    <div class="criteria">
                                        <span class="name">Defense</span>
                                        <span class="value">{{($champion->ratingdefense * 10)}}</span>
                                        <div class="progress">
                                            <div style="width: {{($champion->ratingdefense * 10)}}%;" class="bar"></div>
                                        </div>
                                    </div>
                                    <div class="criteria">
                                        <span class="name">Difficulty</span>
                                        <span class="value">{{($champion->ratingdifficulty * 10)}}</span>
                                        <div class="progress">
                                            <div style="width: {{($champion->ratingdifficulty * 10)}}%;" class="bar"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <p><strong>Base Health: </strong>{{$champion->healthbase}}</p>
                                    <p><strong>Base Health Regen: </strong>{{$champion->healthregenbase}}</p>
                                    <p><strong>Base Mana: </strong>{{$champion->manabase}}</p>
                                    <p><strong>Base Mana Regen: </strong>{{$champion->manaregenbase}}</p>
                                    <p><strong>Base Armor: </strong>{{$champion->armorbase}}</p>
                                    <p><strong>Base Magic Resist: </strong>{{$champion->magicresistbase}}</p>
                                    <p><strong>Base Attack Damage: </strong>{{$champion->attackbase}}</p>
                                    <p><strong>Range: </strong>{{$champion->range}}</p>
                                </div>
                                <div class="span3">
                                    <p><strong>Health per Level: </strong>{{$champion->healthlevel}}</p>
                                    <p><strong>Health Regen per Level: </strong>{{$champion->healthregenlevel}}</p>
                                    <p><strong>Mana per Level: </strong>{{$champion->manalevel}}</p>
                                    <p><strong>Mana Regen per Level: </strong>{{$champion->manaregenlevel}}</p>
                                    <p><strong>Armor per Level: </strong>{{$champion->armorlevel}}</p>
                                    <p><strong>Magic Resist per Level: </strong>{{$champion->magicresistlevel}}</p>
                                    <p><strong>Attack Damage per Level: </strong>{{$champion->attacklevel}}</p>
                                    <p><strong>Movement Speed: </strong>{{$champion->movespeed}}</p>
                                </div>
                            </div>
                            <div id="tab2" class="tab-pane">
                                @foreach ($champion->abilities()->get() as $ability)
                                <div class="full_width">
                                    <h2>@if (!empty($ability->hotkey))
                                        [{{$ability->hotkey}}]
                                        @endif
                                        {{$ability->name}}
                                    </h2>
                                    <div class="thumbnail" style="height: 64px; width: 64px;">
                                        <img alt="{{$ability->name}}" src="{{asset(Utilities::$ability_assets_path . $ability->iconpath)}}">
                                    </div>
                                    <p><strong>Description: </strong>{{$ability->description}}</p>
                                    @if (!empty($ability->effect))
                                    <p><strong>Effect: </strong>{{$ability->effect}}</p>
                                    @endif
                                    @if (!empty($ability->cooldown))
                                    <p><strong>Cooldown: </strong>{{$ability->cooldown}} seconds</p>
                                    @endif
                                    @if (!empty($ability->cost))
                                    <p><strong>Cost: </strong>{{$ability->cost}}</p>
                                    @endif
                                    @if (!empty($ability->range))
                                    <p><strong>Range: </strong>{{$ability->range}}</p>
                                    @endif
                                </div>
                                <div class="separator">&nbsp;</div>
                                @endforeach
                            </div>
                            <div id="tab3" class="tab-pane">
                                <p> <strong>Lore: </strong>{{$champion->description}} </p>
                                <blockquote>{{$champion->quote}}<p> - {{$champion->quoteauthor}}</p></blockquote>
                            </div>
                            <div id="tab4" class="tab-pane">
                                <div id="gallery">
                                    <div data-target="#modal-gallery" data-slideshow="5000" data-toggle="modal-gallery" class="row-fluid" id="gallery-list">
                                        @foreach ($champion->skins() as $skin)
                                        <a rel="gallery" title="@if (empty($skin->displayname))
											Classic {{$champion->displayname}}
											@else
											{{$skin->displayname}}
											@endif" href="{{asset('assets/images/champions/').$skin->splashpath}}" class="gallery-item">
                                            <img alt="Thumbnail" src="{{asset('assets/images/champions/').$skin->portraitpath}}"></a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<div tabindex="-1" class="modal modal-gallery hide fade" id="modal-gallery" style="margin-top: -358px; margin-left: -498.5px; display: none;" aria-hidden="true">
    <div class="modal-header">
        <a data-dismiss="modal" class="close">×</a>
        <h3 class="modal-title" style="width: 953px;"></h3>
    </div>
    <div class="modal-body"><div class="modal-image" style="width: 953px; height: 572px;"><img width="953" height="572" src="" class="in"></div></div>
    <div class="modal-footer">
        <a class="btn btn-primary modal-next">Next <i class="icon-arrow-right icon-white"></i></a>
        <a class="btn btn-info modal-prev"><i class="icon-arrow-left icon-white"></i> Previous</a>
    </div>
</div>
@endsection