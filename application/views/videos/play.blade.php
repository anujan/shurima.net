@layout('master')
@section('title')
{{$video->ign}} playing as {{$video->champion_name}}
@endsection
@section('assets')
@section('description'){{$video->ign}} - {{$video->tier}} {{$video->division}} playing as {{$video->champion_name}}. Recorded on {{$video->updated_at}}.@endsection
@parent
<style type="text/css">
    .tooltip {
        width: 200px;
    }
</style>

<script type="text/javascript">
    $(function () {
        $("[rel='tooltip']").tooltip({placement: 'right'});
    });
</script>
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span10" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('streams')}}">Livestreams</a> <span class="divider">&raquo;</span>
                </li>
                <li class="active typ-pin">{{$video->ign}}</li>
            </ul>
            <div id="post" class="post-lists" style="overflow:hidden;">
                <div class="content-outer">
                    <div class="content-inner">
                        <div class="media-body span9">
                            <h4 class="media-heading">{{$video->ign}} playing as {{$video->champion_name}}</h4>

                            <div class="media" id="stream_player">
                                <?php $archiveId = explode("/", $video->url);
                                $archiveId = $archiveId[count($archiveId) -1];?>

                                <object type="application/x-shockwave-flash" height="70%" width="100%" id="clip_embed_player_flash" data="http://www.justin.tv/widgets/archive_embed_player.swf" bgcolor="#000000">
                                    <param name="movie" value="http://www.justin.tv/widgets/archive_embed_player.swf" />
                                    <param name="allowScriptAccess" value="always" />
                                    <param name="allowNetworking" value="all" />
                                    <param name="allowFullScreen" value="true" />
                                    <param name="flashvars" value="channel={{$video->channel}}&title={{$video->ign}} playing as {{$video->champion_name}}&auto_play=true&archive_id={{$archiveId}}&start_volume=25&initial_time={{$video->time}}" />
                                </object>
                                </object>
                            </div>
                        </div>
                        <?php
                        $runepage = unserialize($video->runepage);
                        $page = unserialize($video->masterypage);
                        ?>
                        <div style="display: block;" class="span10" id="rune_page_view">
                            @include('runepage')
                        </div>
                        <div style="display: block;" class="span10" id="mastery_page_view">
                            @include('masterypage')
                        </div>
                    </div>
                    <!-- content outer -->
                    <script src="{{asset('js/jquery.fitvids.min.js')}}"></script>
                    <script type='text/javascript'>
                        $(document).ready(function(){
                            $("#content").fitVids();
                        });
                    </script>
                </div>
            </div>
    </section>
    <!-- End Main Content -->
</div>
@endsection