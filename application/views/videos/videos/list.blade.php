@layout('master')
@section('title')
Videos
@endsection
@section('description') Shurima.NET's video page shows online videos of players along with information on the streamer such as the champion they're currently playing, runes and masteries being used, and their rank!@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->
    <section class="row" id="main-content">

        <div class="span12" id="content">

            <div id="post" class="post-single">
                <div class="content-outer">
                    <div class="content-inner">
                        <div id="gallery">
                            <form class="form-search" method="POST" action="{{route('video_search')}}">
                                    <input type="text" name="query" class="input-medium search-query" placeholder="Search by champion or streamer...">
                                    <button type="submit" class="btn">Search</button>
                            </form>
                            <div class="separator"></div>
                            <div class="row-fluid" id="gallery-list">
                                <?php $count = 0; ?>
                                <ul>
                                    @foreach ($videos->results as $video)
                                    <?php	$champion = Champion::find($video->champion);?>
                                    <li id="stream_box" class="gallery-item article-thumbnail" rel="gallery">
                                        <a title="{{$video->ign}}" href="{{route('video')}}/{{$video->id}}/{{Str::slug($video->ign." playing as ".$champion->displayname)}}">
                                            <img alt="{{$video->ign}}" src="{{$video->thumbnail}}"></a>
                                        <div id="stream_ign" class="caption">{{$video->ign}}</div>
                                        <div class="stream_info">
                                            <div id="stream_league">{{$video->tier}} {{$video->division}}</div>
                                            @if ($video->champion >= 0)
                                            <div id="stream_champion">
                                            <a href="/champions/{{Str::lower($champion->name)}}" style="color: #AD235E">{{$champion->displayname}}</a>
                                            </div>
                                            @if ($video->matchup != '')
                                            <div id="stream_champion"><a href="#{{$video->id}}Modal" data-toggle="modal" class="btn btn-primary">View Matchup</a></div>
                                            @endif
                                            @endif
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){var e=320;var t=200;$("#gallery-list img").each(function(n){var r=$(this);r.load(function(){if(r.width()<400){e=r.width();t=r.height()}else{r.width(e);r.height(t)}})})})
                    </script>
                    {{$videos->links()}}
                    <!-- content inner -->
                </div>
                <!-- content outer -->
            </div>

        </div>


    </section>
    <!-- End Main Content -->
</div>
@endsection

@section('modal')
@parent
@foreach ($videos->results as $video)
@if ($video->matchup != '')
<div id="{{$video->id}}Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="signInModalLabel">{{$video->ign}}</h3>
    </div>
    <div class="modal-body" style="color:#000">
        <?php $spectate = unserialize($video->matchup); ?>
        <div id="spectate" style="overflow:hidden;">
            <div class="span2">
                @if (isset($spectate['teamone']))
                @foreach ($spectate['teamone'] as $teamone)
                <?php
                if (!isset($teamone['champion']))
                    continue; //It was a ban.
                $t = Champion::find($teamone['champion']);
                ?>
                <p>
                    <a href="http://shurima.net/champions/{{$t->displayname}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                    <strong style="font-size:18px;">{{$teamone['name']}}</strong>
                </p>
                @endforeach
                @endif
            </div>
            <div class="span1" style="margin-top:12%; text-align:center; font-size: 32px;">
                VS
            </div>
            <div class="span2">
                @if (isset($spectate['teamtwo']))
                @foreach ($spectate['teamtwo'] as $teamtwo)
                <?php
                if (!isset($teamtwo['champion']))
                    continue; //It was a ban.
                $t = Champion::find($teamtwo['champion']);
                ?>
                <p>
                    <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                    <strong style="font-size:18px;">{{$teamtwo['name']}}</strong>
                </p>
                @endforeach
                @endif
            </div>
            @if (false === true)
            <div id="bans" style="overflow:hidden;">
                <div class="span3" style="float: left;">
                    @foreach ($spectate['teamone']['bans'] as $ban)
                    <?php
                    $t = DB::connection('sqlite')->table('champions')->find($ban['championId']);
                    ?>
                    <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                    @endforeach
                </div>
                <div class="span3" style="font-size: 32px;">
                    <strong>BANS</strong>
                </div>
                <div class="span3" style="float: left;">
                    @foreach ($spectate['teamtwo']['bans'] as $ban)
                    <?php
                    $t = DB::connection('sqlite')->table('champions')->find($ban['championId']);
                    ?>
                    <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
@endif
@endforeach
@endsection