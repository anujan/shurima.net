@layout('master')
@section('title')
{{$item->name}}
@endsection
@section('description')
{{$item->description}}
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{route('items')}}">Items</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin">{{$item->name}}</li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner" style="overflow: auto;">
            <div class="span1">
                <img title="{{$item->name}}" alt="{{$item->name}}" src="{{asset(Utilities::$item_assets_path.$item->iconpath)}}">
            </div>
            <div class="span4">
            <p><strong>Description: </strong>{{$item->description}}</p>
            <p><strong>Price:</strong>{{$item->getTotalPrice()}}</p></div>
            <?php $q = $item->buildsFrom()->get(); ?>
            @if (count($q) > 0)
            <div id="buildsFrom" class="span7">
                <div class="separator"></div>
                <h3>Builds from:</h3>
                    @foreach ($q as $recipe)
                        @foreach ($recipe->itemFrom()->get() as $b)
                        <?php $name = Str::slug(Str::lower(str_replace("'s", "", $b->name)));?>
                        <div class="span1">
                        <div class="img-frame" style="height: 84px; width: 84px;">
                                <a title="{{$b->name}}" href="/items/{{$name}}" class="hover-item" style="opacity: 1;">
                                        <img alt="{{$b->name}}" src="{{asset(Utilities::$item_assets_path)}}{{$b->iconpath}}">
                                </a>
                        </div>
                        <h4>{{$b->name}}</h4>
                        </div>
                        @endforeach
                    @endforeach
            </div>
            <div class="clearfix"></div>
            @endif
            <?php $t = $item->buildsInto()->get(); ?>
            @if (count($t) > 0)
            <div id="buildsinto" class="span7">
                <div class="separator"></div><h3>Builds into:</h3>
                    <?php $built = array(); ?>
                    @foreach ($t as $recipe)
                        @foreach ($recipe->itemTo()->get() as $b)
                        <?php $name = Str::slug(Str::lower(str_replace("'s", "", $b->name)));
                              if (in_array($b->name, $built)) continue;
                              $built[] = $b->name;
                        ?>
                        <div class="span1">
                        <div class="img-frame" style="height: 84px; width: 84px;">
                                <a title="{{$b->name}}" href="/items/{{$name}}" class="hover-item" style="opacity: 1;">
                                        <img alt="{{$b->name}}" src="{{asset(Utilities::$item_assets_path)}}{{$b->iconpath}}">
                                </a>
                        </div>
                        <h4>{{$b->name}}</h4>
                        </div>
                        @endforeach
                    @endforeach
            </div>
            @endif
                        <!-- content inner -->
                    </div>
                    <!-- content outer -->
                </div>

            </div>


    </section>
    <!-- End Main Content -->

</div>
@endsection