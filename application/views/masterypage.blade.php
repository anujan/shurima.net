<h3>Masteries - {{$page['name']}}</h3>
<div class="separator clearfix" style="margin: 0px;">&nbsp;</div>
<div style="overflow: hidden;">
<div style="background-size: 270px !important; background: url({{asset('img/')}}mastery-tree-1.jpg); float: left; box-shadow: 0pt 0pt 5px rgb(0, 0, 0) inset; border: 2px solid rgb(17, 17, 17);">
    <div style="margin: 16px 16px 6px 16px;">
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][511]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}511.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[511]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][512]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}512.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[512]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][513]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}513.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[513]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][514]}}"  style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}514.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[514]}}/2</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][522]}}"  style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}522.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[522]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][523]}}"  style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}523.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[523]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][524]}}"  style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}524.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[524]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][531]}}"  style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}531.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[531]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][532]}}"  style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}532.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[532]}}</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][533]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}533.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[533]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][541]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}541.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[541]}}/2</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][542]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}542.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[542]}}/2</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][543]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}543.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[543]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][544]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}544.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[544]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][551]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}551.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[551]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][552]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}552.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[552]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][553]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}553.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[553]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][562]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}562.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[562]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
        </div>
    </div>
    <div
        style="text-align: center; padding-top: 0px; font-size: 16px; font: bold 20px 'Trebuchet Ms'; text-shadow: 0 0 2px #000; margin-bottom: 10px;">
        Offense: {{$page[500]}}
    </div>
</div>
<div
    style="background-size: 270px !important; background: url({{asset('img/')}}mastery-tree-2.jpg); float: left; box-shadow: 0pt 0pt 5px rgb(0, 0, 0) inset; border: 3px solid rgb(17, 17, 17);">
    <div style="margin: 16px 16px 6px 16px;">
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][611]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}611.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[611]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][612]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}612.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[612]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][613]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}613.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[613]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][614]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}614.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[614]}}/2</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][621]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}621.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[621]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][622]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}622.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[622]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][624]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}624.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[624]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][631]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}631.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[631]}}/2</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][632]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}632.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[632]}}/2</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][633]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}633.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[633]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][634]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}634.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[634]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][641]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}641.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[641]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][642]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}642.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[642]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][643]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}643.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[643]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][651]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}651.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[651]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][652]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}652.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[652]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="no_rank" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}653.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[653]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][654]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}654.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[654]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][662]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}662.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[662]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
        </div>
    </div>
    <div
        style="text-align: center; padding-top: 0px; font-size: 16px; font: bold 20px 'Trebuchet Ms'; text-shadow: 0 0 2px #000; margin-bottom: 10px;">
        Defense: {{$page[600]}}
    </div>
</div>
<div
    style="background-size: 270px !important; background: url({{asset('img/')}}mastery-tree-3.jpg); float: left; box-shadow: 0pt 0pt 5px rgb(0, 0, 0) inset; border: 4px solid rgb(17, 17, 17);">
    <div style="margin: 16px 16px 6px 16px;">
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][711]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}711.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[711]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][712]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}712.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[712]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][713]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}713.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[713]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][714]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}714.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[714]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][721]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}721.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[721]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][722]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}722.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[722]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][723]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}723.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[723]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][724]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}724.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[724]}}/2</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][731]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}731.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[731]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][732]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}732.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[732]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="no_rank" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}733.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[733]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][734]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}734.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[734]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][741]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}741.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[741]}}/2</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][742]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}742.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[742]}}/4</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][743]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}743.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[743]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][744]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}744.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[744]}}/1</div>
                </div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][751]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}751.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[751]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][752]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}752.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[752]}}/3</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
        </div>
        <div style="padding-bottom: 5px;">
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div class="{{$page['classes'][762]}}" style="position: relative; width: 54px; height: 54px;">
                    <div style="background: url({{asset('img/')}}762.png);" class="mastery_icon">
                        <a href="javascript:;" style="display: inline-block; height: 50px; width: 50px;"></a>
                    </div>
                    <div style="" class="mastery_rank">{{$page[762]}}/1</div>
                </div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
            <div style="display: table-cell; padding: 3px;">
                <div style="width: 54px; height: 54px;"></div>
            </div>
        </div>
    </div>
    <div
        style="text-align: center; padding-top: 0px; font-size: 16px; font: bold 20px 'Trebuchet Ms'; text-shadow: 0 0 2px #000; margin-bottom: 10px;">
        Utility: {{$page[700]}}
    </div>
</div>
</div>