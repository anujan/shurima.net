@layout('master')
@section('title')
ShurimaDB
@endsection
@section('content')
<section class="small_banner">
    <div class="center-wrap">
        <p class="page-banner-heading">ShurimaDB</p>
        <p class="page-banner-description">Select a item or champion to view stats</p>

        <div class="breadcrumbs">
            <a href="/">Home</a> &rarr; <span class="current_crumb">ShurimaDB </span>
        </div>
    </div>

    <div class="shadow top"></div>
    <div class="shadow bottom"></div>
    <div class="tt-overlay"></div>
</section>
<section class="clearfix" id="content-container">
    <div class="clearfix" id="gallery-outer-wrap">
        <div class="clearfix" id="main-wrap">
            {{Utilities::adsense()}}
            <div class="clearfix iso-space isotope" id="iso-wrap" style="overflow: hidden; position: relative;">
                <div class="one_third isotope-item">
                    <div class="img-frame">
                            <a style="opacity: 1;" class="hover-item" href="/champions" title="">
                                <img src="http://shurima.net/assets/images/champions/Elise_Square_0.png" alt="Champions" width="100%" height="100%">
                            </a>
                    </div>
                    <h4>Champions</h4>
                </div>
                <div class="one_third isotope-item">
                    <div class="img-frame">
                            <a style="opacity: 1;" class="hover-item" href="/items" title="">
                                <img src="http://shurima.net/assets/images/items/item6.png" alt="Items" width="100%" height="100%">
                            </a>
                    </div>
                    <h4>Items</h4>
                </div>
            </div>
            <div class="gallery-wp-navi">
            </div>
        </div>
    </div>
</section>
@endsection