﻿<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>@yield('title') - Shurima.NET - Haven for League of Legends</title>
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Anujan Panchadcharam | MrQuackers | Shurima.NET">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @section('assets')
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/shurima.css')}}" title="shurima" />
    <link rel="stylesheet" href="{{asset('css/plugins/camera.css')}}">
    <link rel="shortcut icon" href="{{asset('img/ico/favicon.ico')}}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="{{asset('js/libs/modernizr.js')}}"></script>
    <script>window.jQuery || document.write('<script src="{{asset("js/libs/jquery.js")}}"><\/script>')</script>
    <script src="{{asset('js/shurima.js')}}"></script>
    @yield_section
</head>
<header class="header">
    <div class="container visible-desktop">
        <div class="row">
            <div class="span12" role="navigation" id="topnav">
                <div class="topnav">
                    <div class="pull-left">
                        <span></span>
                    </div>
                    <ul class="pull-right">
                        <li><a href="{{route('contact')}}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="nav" class="container">
        <div class="row">
            <!-- Logo & Navigation -->
            <div class="span12" role="navigation" id="mainnav">
                <!-- Site Logo -->
                <h1 id="logo" class="visible-desktop"><a href="#">Shurima</a></h1>

                <!-- Navigation Start -->
                <nav id="responsive-nav" class="hidden-desktop">
                    <div class="collapse-menu">
                        <a href="#" class="brand">Shurima</a>
                        <form class="search">
                            <input class="input-medium" type="text" placeholder="Search&hellip;">
                        </form>
                        <ul class="user-menu">
                            @if (Auth::guest())
                            <li><a href="#signInModal" data-toggle="modal">Sign In</a></li>
                            <li><a href="{{action('elojournal@register')}}">Sign Up</a></li>
                            @else
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account
                                    <span class="awe-caret-down icon-white"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{action('elojournal@settings')}}"><span class="awe-cog"></span> Settings</a></li>
                                    <li><a href="{{action('elojournal@logout')}}"><span class="awe-signout"></span> Sign Out</a></li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                        <span class="collapse-trigger icon awe-chevron-down"></span>
                    </div>
                    <ul class="first-level dropdown-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="{{action('streams')}}">Livestreams</a></li>
                        <li><a href="{{action('videos')}}">Video Archive</a></li>
                        <li><a href="{{action('elojournal')}}">EloJournal
                            @if (Auth::check())<span class="pull-right icon awe-chevron-down"></span></a>
                            <ul class="second-level">
                                <li><a href="#shareModal" data-toggle="modal">Share</a></li>
                                <li><a href="{{action('elojournal@stats')}}">Stats</a></li>
                            </ul>
                            @else
                            </a>
                            @endif
                        </li>
                        <li><a href="{{action('blog')}}">Blog</a>
                            @if (Auth::check() && Auth::user()->role > 1)
                            <ul class="second-level">
                                <li><a href="{{action('blog@add')}}">Add Post</a></li>
                            </ul>
                            @endif
                        </li>
                        <li><a href="{{action('spectate')}}">Spectate</a></li>
                        <li><a href="#">ShurimaDB <span class="pull-right icon awe-chevron-down"></span></a>
                            <ul class="second-level">
                                <li><a href="{{route('champions')}}">Champions</a></li>
                                <li><a href="{{route('items')}}">Items</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">Tools <span class="pull-right icon awe-chevron-down"></span></a>
                            <ul class="second-level">
                                <li><a href="{{route('tools')}}/recommended">Recommended Item Changer</a></li>
                                <li><a href="{{route('tools')}}/pickacard">Pick a Card simulator</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <nav id="main-navigation" class="visible-desktop">
                    <ul class="first-level">
                        <li class="current"><a href="/">Home</a></li>
                        <li><a href="{{action('streams')}}">Livestreams</a></li>
                        <li><a href="{{action('videos')}}">Video Archive</a></li>
                        <li><a href="{{action('elojournal')}}">EloJournal
                            @if (Auth::check())<span class="icon awe-chevron-down"></span></a>
                            <ul class="second-level">
                                <li><a href="#shareModal" data-toggle="modal">Share</a></li>
                                <li><a href="{{action('elojournal@stats')}}">Stats</a></li>
                            </ul>
                            @else
                            </a>
                            @endif
                        </li>
                        <li><a href="{{action('blog')}}">Blog</a>
                                @if (Auth::check() && Auth::user()->role > 0)
                                <ul class="second-level">
                                    <li><a href="{{action('blog@add')}}">Add Post</a></li>
                                </ul>
                                @endif
                        </li>
                        <li><a href="{{action('spectate')}}">Spectate</a></li>
                        <li><a href="#">ShurimaDB <span class="icon awe-chevron-down"></span></a>
                            <ul class="second-level">
                                <li><a href="{{route('champions')}}">Champions</a></li>
                                <li><a href="{{route('items')}}">Items</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <nav id="secondary-navigation" class="visible-desktop">
                    <div class="social-media">
                        <ul>
                            <li><a href="http://facebook.com/ShurimaNetwork"><span class="awe-facebook"></span></a></li>
                            <li><a href="https://twitter.com/ShurimaNetwork"><span class="awe-twitter"></span></a></li>
                        </ul>
                    </div>
                    <div class="pull-right">
                        <ul>
                            <li>
                                <ul>
                                    @if (Auth::guest())
                                    <li><a href="#signInModal" data-toggle="modal">Sign In</a></li>
                                    <li><a href="{{action('elojournal@register')}}" style="max-height:18px">Sign Up</a></li>
                                    @else
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account
                                            <span class="awe-caret-down icon-white"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{action('elojournal@settings')}}"><span class="awe-cog"></span> Settings</a></li>
                                            <li><a href="{{action('elojournal@logout')}}"><span class="awe-signout"></span> Sign Out</a></li>
                                        </ul>
                                    </li>
                                    @endif
                                </ul>
                            </li>
                            <!--
                            <li>
                                <form class="search">
                                    <input class="input-medium" type="text" placeholder="Search&hellip;">
                                </form>
                            </li>
                            -->
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
@yield('content')
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39044304-1']);
  _gaq.push(['_setDomainName', 'shurima.net']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<footer class="footer">
<div class="container footer-container">
<section class="row">
<div class="span4 visible-desktop">
    <div class="row footer-left">
        <!-- Begin Span4 Carousel Slider -->
        <div class="span4 carousel-widget">
            <div class="widget-header">
                <h4 class="widget-heading">Latest From Blog</h4>
            </div>
            <div class="widget-content carousel slide" id="carousel-widget2">
                <!-- Begin Carousel Inner -->
                <div class="carousel-inner">
                    <!-- Begin Slide Section / Carousel Item -->
                    <?php $blogPosts = Post::order_by('updated_at', 'desc')->take(15)->get(); ?>
                    @for ($i = 0; $i < count($blogPosts)/3; $i++)
                    <div class="slide-section @if($i == 0) active @endif item">
                        @for ($j = 0; $j < 3; $j++)
                            @if (isset($blogPosts[(3 * $i) + $j]))
                                <?php $blogPost = $blogPosts[(3 * $i) + $j]; ?>
                                <div class="separator"></div>
                                <div class="slide-item media">
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="{{action('blog@post')}}/{{$blogPost->id}}">{{$blogPost->title}}</a></h4>
                                        <p>{{Str::limit(strip_tags($blogPost->content), 100)}}
                                        </p>
                                    </div>
                                </div>
                            @endif
                        @endfor
                    </div>
                    @endfor
                </div>
                <!-- End Carousel Inner -->
            </div>
        </div>
        <!-- End Span 4 Carousel Slider -->
    </div>
</div>

<div class="span3 footer-extra">
    <div class="footer-box">
        <h4>Livestreams</h4>
        <ul>
            @foreach (Utilities::getLiveStreams(8) as $s)
            <li><a href="{{action('streams')}}/{{$s->id}}/{{Str::slug($s->ign)}}">{{$s->ign}}
                    @if ($s->champion >=0)
                    - {{Champion::find($s->champion)->displayname}}
                    @endif</a></li>
            @endforeach
        </ul>
    </div>
    <div class="footer-box">
        <h4>Latest Videos</h4>
        <ul>
            @foreach (Video::where('pending', '=', 0)->order_by('updated_at', 'desc')->take(8)->get() as $video)
            <li><a href="{{route('video')}}/{{$video->id}}/{{Str::slug($video->ign." playing as ".$video->champion_name)}}">{{$video->ign}} - {{$video->champion_name}}</a></li>
            @endforeach
        </ul>
    </div>
</div>
<div class="span3 footer-extra">
    <div class="footer-box">
        <h4>About Us</h4>
        <p><a href="#"><img src="{{asset('img/assets/logo.png')}}" alt="Shurima" ></a></p>
        <p class="color"><em>Shurima is a League of Legends fansite that provides tools focused on helping players improve at the game. It provides champion and item information as well as pro player videos and setups. It also features Elojournal which allows summoners to monitor their progress in ranked games. We also store tons of videos of streamers and have the best livestream system out there!</em></p>
        <address>
            <p><strong>Contact Support</strong></p>
            <p><span class="awe-envelope-alt"></span><a href="mailto:support@shurima.net"> support@shurima.net</a></p>
        </address>
    </div>
</div>
<div class="span2 footer-extra">
    <div class="footer-box">
        <h4>Quick Links</h4>
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="{{action('elojournal')}}">EloJournal</a></li>
            <li><a href="{{action('spectate')}}">Spectate</a></li>
            <li><a href="{{action('streams')}}">Livestreams</a></li>
            <li><a href="{{action('videos')}}">Video Archive</a></li>
        </ul>
    </div>
    <div class="footer-box">
        <h4>Follow Us</h4>
        <ul class="social-media">
            <li><a href="http://facebook.com/ShurimaNetwork"><span class="awe-facebook"></span></a></li>
            <li><a href="https://twitter.com/ShurimaNetwork"><span class="awe-twitter"></span></a></li>
        </ul>
    </div>
</div>
</section>

</div>
<div class="container" role="navigation">
    <section class="row">
        <div class="footer-menu">
            <ul>
                <li><a href="#" title="link">About Us</a></li>
                <li><a href="{{route('contact')}}" title="link">Contact Us</a></li>
                <li><a href="#" title="link">Term of Services</a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>Copyright &copy; 2012 - 2013 <a href="#" title="shurima.net">Shurima</a>. All rights reserved.
            </p>
        </div>
    </section>
</div>
</footer>
<!-- Footer End -->
<!-- Modal -->
@section('modal')
@if (Auth::guest())
<div id="signInModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="signInModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="signInModalLabel">Sign In</h3>
    </div>
    <div class="modal-body">
        <form method="post" action="{{action('elojournal@login')}}" name="login_form">
            <p><input type="text" class="span3" name="username" id="email" placeholder="Username"></p>
            <p><input type="password" class="span3" name="password" placeholder="Password"></p>
            <p><button type="submit" class="btn btn-primary">Sign in</button>
                <a href="{{action('elojournal@forgotpassword')}}">Forgot Password?</a>
            </p>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
@else
<div id="shareModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="signInModalLabel">Share your EloJournal</h3>
    </div>
    <div class="modal-body" style="color:#000">
        <h2>Your share link is: <a href="http://shurima.net/elojournal/view/{{Auth::user()->getShareKey()}}" alt="Share Link">http://shurima.net/elojournal/view/{{Auth::user()->getShareKey()}}</a></h2>
        <p>Copy it and share it with your friends!</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
@endif
@yield_section
</body>
</html>