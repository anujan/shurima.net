@layout('master');
@section('title')
Contact Us
@endsection
@section('description') Shurima is a League of Legends fansite that provides tools focused on helping players improve at the game. It provides champion and item information as well as pro player videos and setups. It also features Elojournal which allows summoners to monitor their progress in ranked games. @endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="activetyp-pin"><a href="{{route('contact')}}">Contact Us</a></li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        <h3>Contact Us</h3>
                        <div class="separator"></div>
                        @if (isset($success))
                            <div class="alert alert-success">{{$success}}</div>
                        @endif
                        <form method="post" action="" name="contactform" id="contactform">
                            <fieldset>
                                <label for="name" accesskey="U"><span>*</span> Your Name</label>
                                <input name="name" type="text" id="name" size="30" value=""/>
                                <br/>
                                <label for="email" accesskey="E"><span>*</span> Email</label>
                                <input name="email" type="text" id="email" size="30" value=""/>
                                <br/>
                                <label for="comments" accesskey="C"><span>*</span> Message</label>
                                <textarea name="comments" cols="40" rows="3" id="comments"></textarea>
                                <br/>
                                <div class="tt-form-verify">
                                    <p><span class="required">*</span> <strong>Form Verification.</strong><br/><em>(enter
                                            the verfication code in the field below)</em></p>
                                    <label for="verify" accesskey="V">&nbsp;&nbsp;&nbsp;<img
                                            src="{{LaraCaptcha\Captcha::img()}}" alt="Image verification"
                                            border="0"/></label>
                                    <input name="verify" type="text" id="verify" size="6" value=""
                                           style="width: 90px;"/>
                                </div>
                                <input type="submit" class="btn btn-flat btn-success" id="submit" value="Submit"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    @endsection