@layout('master');
@section('title')
EloJournal - FAQ
@endsection

@section('content')
@section('content')
<section class="small_banner">
        <div class="center-wrap">
                <p class="page-banner-heading">FAQ</p>
                <div class="breadcrumbs">
                        <a href="/">Home</a> &rarr; <a href="{{action('elojournal@index')}}">EloJournal</a> &rarr; <span class="current_crumb">FAQ</span>
                </div>
        </div>
        <div class="shadow top"></div>
        <div class="shadow bottom"></div>
        <div class="tt-overlay"></div>
</section>
<section id="content-container" class="clearfix">
        <div id="main-wrap" class="clearfix">
                {{Utilities::adsense()}}
                <div class="faq-questions">
                        <div class="li_container" id="li_container1"><ol class="li_cont1" style="float: left; width: 452px;">
                                        <li class="li_col1" value="1"><a href="#" onclick="return false;" id="question_1">What is EloJournal?</a></li>
                                        <li class="li_col1" value="2"><a href="#" onclick="return false;" id="question_2">How do I signup?</a></li>
                                        <li class="li_col1" value="3"><a href="#" onclick="return false;" id="question_3">Is it free?</a></li>
                                        <li class="li_col1" value="4"><a href="#" onclick="return false;" id="question_4">I found a bug, who do I contact?</a></li>
                                        <li class="li_col1" value="5"><a href="#" onclick="return false;" id="question_5">Why haven't my games updated yet?</a></li>
                                        <li class="li_col1" value="6"><a href="#" onclick="return false;" id="question_6">How come I'm missing some games?</a></li>
                                        <li class="li_col1" value="7"><a href="#" onclick="return false;" id="question_7">Can you add the _____ stat?</a></li>
                                        <li class="li_col1" value="8"><a href="#" onclick="return false;" id="question_8">Are you planning to have support for other regions/servers?</a></li>
                                        <li class="li_col1" value="9"><a href="#" onclick="return false;" id="question_9">Do you only log Ranked Solo queue games?</a></li>
                                </ol></div>
                </div><div class="faq-answers clearfix">
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_1"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_1" class="faq-heading">What is EloJournal?</p>
                                        <div id="answer_1_text" class="faq-answer">
                                                <p>EloJournal is a log of all your Ranked Games. It allows you to keep track of all your games, and make notes on how the game was. Your note can be anything from what you need to improve on to what you did well.</p>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_2"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_2" class="faq-heading">How do I signup?</p>
                                        <div id="answer_2_text" class="faq-answer">
                                                <p>You sign up by clicking the Register button in the EloJournal dropdown navigation panel. Just fill out some information such as your server, summoner name and account information for the site. This is not your League of Legends account information. Please use a seperate Username and Password from your League of Legends account.</p>
                                                <p>After you register, you will be told how to verify ownership of your account. If you have problems verifying, feel free to contact us!</p>
                                                <a class="small blue button" href="{{action('elojournal@register')}}">Register now!</a>&nbsp;<a class="small green button" href="{{action('elojournal@login')}}">Login!</a>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_3"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_3" class="faq-heading">Is it free?</p>
                                        <div id="answer_3_text" class="faq-answer">
                                                <p>Yes. EloJournal is completely free. Although, in order to have our servers update your stats as often as we do, we ask that users turn off ad-block. This isn't mandatory but it would help us a lot.</p>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_4"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_4" class="faq-heading">I found a bug, or have a suggestion. Who do I report it to?</p>
                                        <div id="answer_4_text" class="faq-answer">
                                                <p>We have several ways for you to contact us! You can contact us by tweeting us on Twitter, messaging us on Facebook, messaging MrQuackers on Reddit or emailiing admin@shurima.net!
                                                </p>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_5"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_5" class="faq-heading">Why haven't my games updated yet?</p>
                                        <div id="answer_5_text" class="faq-answer">
                                                <p>If your games haven't updated since you've logged in, our servers are most likely down. Since it's a free service, our server stability isn't as good as we'd want it to be. It's usually not down for too long, but if it is, we'd appreciate if you contacted us!</p>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_6"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_6" class="faq-heading">How come I'm missing games?</p>
                                        <div id="answer_6_text" class="faq-answer">
                                                <p>Games update when you are logged in to your EloJournal account. Unfortunately, when we update we can only retrieve the games in your match history, so if you haven't logged in a while, matches that aren't on your match history will be unretrievable.</p>
                                                <p>In order to keep all your games logged, we recommended you log in to EloJournal daily.</p>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_7"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_7" class="faq-heading">Can you add the ______ stat?</p>
                                        <div id="answer_7_text" class="faq-answer">
                                                <p>Sure! We have all the stats logged.</p>
                                                <p>If you would like games to show other stats that would help you improve, let us know through our contact us form, or one of our social media accounts!</p>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_8"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_8" class="faq-heading">Are you planning to have support for other regions?</p>
                                        <div id="answer_8_text" class="faq-answer">
                                                <p>Yes! We plan to add support for other regions eventually but for now we can't afford to.</p>
                                                <p>If you would like your server added, please contact us. We do plan on having support for all servers eventually, but we want to add servers with more demand first. By contacting us, it helps us get a rough estimate on how many users per server will be using our service.</p>
                                        </div>
                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                        <div class="faq-wrap clearfix">
                                <div class="faq-number answer_9"></div>
                                <!-- END FAQ-number -->      
                                <div class="faq-content">
                                        <p id="answer_9" class="faq-heading">Do you only log ranked solo queue games?</p>
                                        <div id="answer_9_text" class="faq-answer">
                                                <p>Yes. We plan to add ranked teams support later on, but not for a while, due to our resources being limited at the moment.</p>
                                        </div>

                                        <div class="basic-divider"><a onclick="return false;" href="#" class="go_to_top">top</a></div>
                                </div>
                                <!-- END faq-content -->
                        </div>
                        <!-- END faq-wrap -->
                </div>
                <script src="{{asset('js/custom-faq.js')}}" type="text/javascript"></script>
                <script src="{{asset('js/jquery.highlightFade.js')}}" type="text/javascript"></script>
                <script type="text/javascript" src="{{asset('js/jquery.scrollTo-min.js')}}"></script>
        </div>
</section>
@endsection