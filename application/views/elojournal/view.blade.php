@layout('master')

@section('title')
Elojournal
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span10" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin"><a href="{{action('elojournal')}}">EloJournal</a></li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        <div id="games">
                            @foreach ($paginate->results as $g)
                            <article id="game-{{$g->id}}" style="overflow:hidden;">
                                {{Utilities::getGameHTML($g, false)}}
                            </article>
                            <div class="separator">&nbsp;</div>
                            @endforeach
                            {{$paginate->links()}}
                           </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection