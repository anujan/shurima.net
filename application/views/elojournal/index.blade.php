@layout('master')

@section('title')
Elojournal
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
    <div class="@if (Auth::guest()) span8 @else span10 @endif" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin"><a href="{{action('elojournal')}}">EloJournal</a></li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                    @if (Auth::guest())
                    <article>
                    <div class="article-header">
                        <h1 class="title">Track all your games with EloJournal!</h1>
                        <div class="separator"></div>
                    </div>
                    <div class="article-content">
                        <blockquote class="post-lead">EloJournal is place to keep track of all your game experiences in League of Legends. This allows you to monitor your progress and keep a clear view on what you need to improve on.</blockquote>
                        <h3 class="post-lead">Features</h3>

                        <ul>
                            <li>Logs and retrieves all your ranked games when you visit your EloJournal page</li>
                            <li>Saves all games forever rather than just the last 10 in your match history</li>
                            <li>Allows you to write notes on each game</li>
                            <li>Gives you your own url so you can share your page and progress with your friends</li>
                        </ul>

                        <p>
                            If you have any feature suggestions, feel free to tweet at us!
                        </p>

                        <h3 class="post-lead">Statistics Logged</h3>
                        <?php $eloStats = Utilities::getEloJournalStats(); ?>
                        <p><strong>Total Users: </strong> {{$eloStats['users']}}</p>
                        <p><strong>Total Games: </strong> {{$eloStats['games']}}</p>
                        <p><strong>Total Wins: </strong> {{$eloStats['wins']}}</p>
                        <p><strong>Total Losses: </strong> {{$eloStats['losses']}}</p>
                        <p><strong>Win Rate: </strong> {{number_format($eloStats['winrate'], 3)}}</p>
                        <p><strong>Most played champion: </strong> {{$eloStats['mostplayedchampion']}}</p>
                        <p><strong>Highest win rate champion: </strong> {{$eloStats['highestwinrate']}}</p>
                        <p><strong>Lowest win rate champion: </strong> {{$eloStats['lowestwinrate']}}</p>
                    </div>
                    <div class="separator"></div>
                    </article>
                    @else
                        <h4 id="loading">Checking for new games...</h4>
                        <div id="games">
                            @foreach ($paginate->results as $g)
                            <article id="game-{{$g->id}}" style="overflow:hidden;">
                                {{Utilities::getGameHTML($g)}}
                            </article>
                            <div class="separator">&nbsp;</div>
                            @endforeach
                            {{$paginate->links()}}
                            <script type="text/javascript">eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('$.4({3:"8://7.9/d/n"}).c(0(1){$("#j").i(1);$("#h").5()});0 k(2){g r=f("e 6 s 6 q p b o a?");m(r==l){$.4({3:"8://7.9/d/b/"+2}).c(0(1){$("#a-"+2).5()})}}',29,29,'function|msg|id|url|ajax|remove|you|shurima|http|net|game|delete|done|elojournal|Are|confirm|var|loading|prepend|games|rem|true|if|update|this|to|want||sure'.split('|'),0,{}))</script>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="span3" id="right-sidebar">
            @if (Auth::guest())
            <div class="sidebar-widget sidebar-block sidebar-color">
                <div class="sidebar-header">
                    <h4>Login Form</h4>
                </div>
                <div class="sidebar-content login-widget">
                    <form action="{{action('elojournal@login')}}" method="POST">
                        <div class="input-prepend">
                            <span class="add-on "><i class="icon-user"></i></span>
                            <input type="text" placeholder="username" name="username" class="span2">
                        </div>
                        <div class="input-prepend">
                            <span class="add-on "><i class="icon-lock"></i></span>
                            <input type="password" name="password" placeholder="password" class="span2">
                        </div>
                        <label class="checkbox">
                            <input type="checkbox" name="remember">
                            Remember me
                        </label>
                        <div class="controls">
                            <button type="submit" class="btn signin">Sign In</button>
                            <button class="btn signup" href="{{action('elojournal@register')}}">Sign up</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
        </div>
    </section>
</div>
@endsection