@layout('master')

@section('title')
EloJournal - Settings
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.
@endsection
@section('content')
<section class="small_banner">
        <div class="center-wrap">
                <p class="page-banner-heading">Settings</p>
                <div class="breadcrumbs">
                        <a href="/">Home</a> &rarr; <a href="{{action('elojournal@index')}}">EloJournal</a> &rarr; <span class="current_crumb">Settings</span>
                </div>
        </div>
        <div class="shadow top"></div>
        <div class="shadow bottom"></div>
        <div class="tt-overlay"></div>
</section>
<section class="clearfix" id="content-container">
        <div class="clearfix" id="main-wrap">   
                <div class="full_width">
                        {{Utilities::adsense()}}
                        <?php
                                $key = Auth::user()->getShareKey();
                        ?>
                        <h2>Your share link is: <a href="http://shurima.net/elojournal/view/{{$key}}" alt="Share Link">http://shurima.net/elojournal/view/{{$key}}</a></h2>
                        <p>Copy it and share it with your friends!</p>
                </div>
        </div>
</section>
@endsection