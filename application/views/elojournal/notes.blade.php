@layout('master')

@section('title')
Note - Elojournal
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.
@endsection
@section('assets')
    @parent
    <script src="{{asset('js/wysihtml5-0.3.0.min')}}"></script>
    <script src="{{asset('js/bootstrap-wysihtml5-0.0.2.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-wysihtml5-0.0.2.css')}}">
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span10" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('elojournal')}}">EloJournal</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin active">Note</li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        <div id="game" style="overflow:hidden;">
                        {{Utilities::getGameHTML($game, false)}}

                        {{Utilities::adsense()}}
                        </div>
                        <div class="separator">&nbsp;</div>
                        <div style="width:100%">
                        @if (isset($error))
                        <div class="alert alert-error">
                            <strong>Error!</strong> {{$error}}
                        </div>
                        @endif
                        <br/>
                        {{Form::open()}}
                        {{Form::textarea('note', $game->note, array('autofocus' => 'autofocus', 'style' => 'width: 100%;'))}}
                        <br/>
                        {{Form::submit("Submit")}}
                        {{Form::close()}}
                        <script>
                            $(function () {
                                $("textarea").wysihtml5();
                            });
                        </script>
                        </div>

                    </div>
                </div>
            </div>
    </section>
</div>
@endsection