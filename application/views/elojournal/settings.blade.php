@layout('master')

@section('title')
Settings - Elojournal
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('elojournal')}}">EloJournal</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin">Settings</li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        {{Utilities::adsense()}}
                        @if (isset($error_message))
                        <div class="alert alert-error">
                                <strong>Error!</strong> {{$error_message}}
                        </div>
                        @endif
                        {{Form::open()}}
                        {{Form::label('username', 'Username')}}
                        {{Form::text('username', Auth::user()->username, array('disabled' => 'disabled'))}}
                        {{Form::label('email', 'Email')}}
                        {{Form::text('email', Auth::user()->email)}}
                        {{Form::label('password', 'Password')}}
                        {{Form::password('password')}}
                        {{Form::label('password_confirmation', 'Password Confirmation')}}
                        {{Form::password('password_confirmation')}}
                        {{Form::label('summoner', 'Summoner Name')}}
                        {{Form::text('summoner', Auth::user()->summoner)}}
                        @if (Auth::user()->role > 0)
                        {{Form::label('about', 'About Author')}}
                        {{Form::text('about', Auth::user()->about)}}
                        @endif
                        {{Form::label('server', 'Server')}}
                        {{Form::select('server', array('NA' => "North America", 'EUW' => 'Europe West', 'EUNE' => 'Europe Nordic East', 'BR' => 'Brazil', 'TR' => 'Turkey', 'SG' => 'Garena SG/MY/SEA', 'TW' => 'Garena TW'), Str::upper(Auth::user()->server))}}
                        <br>
                        {{Form::submit("Submit", array('class' => 'btn'))}}
                        {{Form::close()}}
                    </div>
                </div>
            </div>
    </section>
</div>
@endsection