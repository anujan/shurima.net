@layout('master')

@section('title')
EloJournal - Forgot Password
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('elojournal')}}">EloJournal</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin">Forgot Password</li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        @if (isset($error))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Error!</strong> {{$error}}
                        </div>
                        @endif
                        @if (isset($success))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Error!</strong> {{$success}}
                        </div>
                        @endif
                        {{Form::open('http://shurima.net/elojournal/forgotpassword')}}
                        {{Form::label('email', 'Username/Email Address')}}
                        {{Form::text('email')}}
                        {{Form::submit("Send me my password")}}
                        {{Form::close()}}

                    </div>
                </div>
    </section>
    @endsection