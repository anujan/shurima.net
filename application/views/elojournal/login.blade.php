@layout('master');
@section('title')
Login - Elojournal
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.@endsection

@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('elojournal')}}">EloJournal</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin">Login</li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        @if (isset($login_errors))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Error!</strong> The login credentials you've provided do not match.
                        </div>
                        @endif

                        <h4>Login</h4>
                        <form class="form-horizontal" method="post">
                            <div class="control-group">
                                <label class="control-label" for="inputEmail">Email</label>
                                <div class="controls">
                                    <input id="inputEmail" placeholder="Email" name="username" type="text">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputPassword">Password</label>
                                <div class="controls">
                                    <input id="inputPassword" placeholder="Password" name="password" type="password">
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <label class="checkbox">
                                        <input type="checkbox"> Remember me
                                    </label>
                                    <button type="submit" class="btn">Sign in</button>
                                    <p></p><a href="{{action('elojournal@register')}}" title="Register" alt="Register">Don't have an account? Click here to register!</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
        </div>
    </div>
</section>
@endsection