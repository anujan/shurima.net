@layout('master')

@section('title')
Stats- Elojournal
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.
@endsection
@section('assets')
@parent
<script type="text/javascript">
    $(function () {
        $("[rel='tooltip']").tooltip();
    });
</script>
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('elojournal')}}">EloJournal</a></li>
                <li class="active typ-pin"><a href="{{action('elojournal@stats')}}">Stats</a></li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        <table class="table table-hover table-bordered table-striped" style="color: #000">
                            <thead>
                            <tr>
                                <th>Champion</th>
                                <th><a href="#" rel="tooltip" title="Your win rate with this champion">Win rate as you</a></th>
                                <th><a href="#" rel="tooltip" title="Your win rate with this champion as an ally">Win rate as ally</a></th>
                                <th><a href="#" rel="tooltip" title="Your win rate against this champion">Win rate as enemy</a></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($stats as $stat)
                            <tr>
                                <td>{{$stat[10]}}</td>
                                <td>{{$stat[2]}}</td>
                                <td>{{$stat[5]}}</td>
                                <td>{{$stat[8]}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="span3" id="right-sidebar">
            <div class="sidebar-widget">
                <a class="twitter-timeline"  href="https://twitter.com/ShurimaNetwork"  data-widget-id="314098688756682753">Tweets by @ShurimaNetwork</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
        </div>
    </section>
</div>
@endsection