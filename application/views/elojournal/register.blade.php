@layout('master')

@section('title')
EloJournal - Register
@endsection
@section('description')
Elojournal is a League of Legends tool that allows summoners to monitor their progress and keep a clear view on what needs to be improved on in ranked matches. It logs all their ranked games and stats for them and allows them to write notes and share the page wtih their peers.
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('elojournal')}}">EloJournal</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin">Register</li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        @if (isset($register_error))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Error!</strong> {{$register_error}}
                        </div>
                        @endif

                        {{Form::open()}}
                        {{Form::label('username', 'Username')}}
                        {{Form::text('username')}}
                        {{Form::label('email', 'Email')}}
                        {{Form::text('email')}}
                        {{Form::label('password', 'Password')}}
                        {{Form::password('password')}}
                        {{Form::label('password_confirmation', 'Password Confirmation')}}
                        {{Form::password('password_confirmation')}}
                        {{Form::label('summoner', 'Summoner Name')}}
                        {{Form::text('summoner')}}
                        {{Form::label('server', 'Server')}}
                        {{Form::select('server', array('NA' => "North America", 'EUW' => 'Europe West', 'EUNE' => 'Europe Nordic East', 'BR' => 'Brazil', 'TR' => 'Turkey', 'SG' => 'Garena SG/MY/SEA', 'TW' => 'Garena TW'))}}
                        <div class="tt-form-verify">
                                <p><span class="required">*</span> <strong>Form Verification.</strong><br /><em>(enter the verfication code in the field below)</em></p>
                                <label for="verify" accesskey="V">&nbsp;&nbsp;&nbsp;<img src="{{LaraCaptcha\Captcha::img()}}" alt="Image verification" border="0"/></label>
                                <input name="verify" type="text" id="verify" size="6" style="width: 90px;" />
                        </div>
                        {{Form::submit("Register")}}
                        {{Form::close()}}

                    </div>
                </div>
    </section>
@endsection