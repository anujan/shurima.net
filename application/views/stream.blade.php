@layout('master')
@section('title')
{{$stream->ign}} - Livestreams
@endsection
@section('assets')
@section('description'){{$stream->ign}} - {{$stream->description}}@endsection
@parent
<style type="text/css">
    .tooltip {
        width: 200px;
    }
</style>
<script src="{{asset('js/jquery.fitvids.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
        $("[rel='tooltip']").tooltip({placement: 'right'});
        $("#content").fitVids();
        setInterval(function (){
            $('#rune_page_view').load("http://shurima.net/runepage/{{$stream->id}}");
            $('#mastey_page_view').load("http://shurima.net/mastery/{{$stream->id}}");
        }, 300000);
    });
</script>
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span10" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('streams')}}">Livestreams</a> <span class="divider">&raquo;</span>
                </li>
                <li class="active typ-pin">{{$stream->ign}}</li>
            </ul>
            <div id="post" class="post-lists" style="overflow:hidden;">
                <div class="content-outer">
                    <div class="content-inner">
                        <div class="media-body span9">
                            <h4 class="media-heading">{{$stream->title}}</h4>

                            <div class="media" id="stream_player">
                                <object type="application/x-shockwave-flash"
                                        id="live_embed_player_flash"
                                        height="70%"
                                        width="100%"
                                        data="http://www.twitch.tv/widgets/live_embed_player.swf?channel={{$stream->channel}}"
                                        bgcolor="#000000">
                                    <param name="allowFullScreen"
                                           value="true"/>
                                    <param name="allowScriptAccess"
                                           value="always"/>
                                    <param name="allowNetworking"
                                           value="all"/>
                                    <param name="movie"
                                           value="http://www.twitch.tv/widgets/live_embed_player.swf"/>
                                    <param name="flashvars"
                                           value="hostname=www.twitch.tv&channel={{$stream->channel}}&auto_play=true&start_volume=50"/>
                                </object>
                            </div>
                        </div>
                        <?php
                        $runepage = empty($stream->runepage) ? Utilities::getCurrentRunepage($stream->region, $stream->acctId) : unserialize($stream->runepage);
                        $page = empty($stream->masterypage) ? Utilities::getCurrentMasteryPage($stream->region, $stream->summonerid) : unserialize($stream->masterypage);
                        ?>
                        <div style="display: block;" class="span10" id="rune_page_view">
                            @include('runepage')
                        </div>
                        <div style="display: block;" class="span10" id="mastery_page_view">
                            @include('masterypage')
                        </div>
                    </div>
                    <!-- content outer -->
                </div>
            </div>

            <!-- Sidebar Right -->
            <div id="right-sidebar" style="display:none;">
                <div id="latest-reviews" class="sidebar-widget">
                    <div class="sidebar-header">
                        <h4>Livestreams</h4>

                        <div class="separator"></div>
                    </div>
                    <div class="sidebar-content post-widget">
                        <ul>
                            @foreach (Utilities::getLiveStreams(10) as $s)
                            <li class="sidebar-item">
                                <a href="{{action('streams')}}/{{$s->id}}/{{Str::slug($s->ign)}}" class="image-polaroid"
                                   title="{{$s->ign}}">
                                    <img src="{{str_replace("320x200", "60x60",$s->thumbnail)}}" alt="{{$s->ign}}" />
                                </a>
                                <h5><a href="{{action('streams')}}/{{$s->id}}/{{Str::slug($s->ign)}}">{{$s->ign}}
                                        @if ($s->champion >=0)
                                        - {{Champion::find($s->champion)->displayname}}
                                        @endif</a></h5>
                                <span class="post-date"><em>{{$s->viewers}} Viewers</em></span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Sidebar End -->
    </section>
    <!-- End Main Content -->
</div>
@endsection