@layout('master')
@section('title')
Team Members
@endsection

@section('content')
     <section class="small_banner">
         <div class="center-wrap">
            <p class="page-banner-heading">Team Members</p>
            <!-- END banner-search -->
            <div class="breadcrumbs"><a href="/">Home</a> &rarr; <span class="current_crumb">Team Members </span></div>
            <!-- END breadcrumbs -->
         </div>
         <!-- END center-wrap -->
         <div class="shadow top"></div>
         <div class="shadow bottom"></div>
         <div class="tt-overlay"></div>
      </section>
      <section id="content-container" class="clearfix">
         <div id="main-wrap" class="clearfix">
            <div class="member-wrap clearfix">
               <div class="member-contact">
                  <p class="member-name">Anujan (MrQuackers)</p>
                  <p class="member-title">Administrator, developer</p>
                  <ul class="member-list">
                     <li><a href="mailto:admin@shurima.net" class="member-contact-email">admin@shurima.net</a></li>
                     <li><a href="http://www.twitter.com/shurimanetwork" class="member-contact-twitter">@ShurimaNetwork</a></li>
                  </ul>
               </div>
               <!-- END member-contact -->
               <div class="member-bio">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras mattis consectetur purus sit amet fermentum. Donec ullamcorper nulla non metus auctor fringilla. Donec ullamcorper nulla non metus auctor fringilla.</p>
                  <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>
               </div>
               <!-- END member-bio -->
               <div class="member-photo img-frame member-frame"><img src="content-images/sample-bio-1.png" /></div>
            </div>
            <!-- END member-wrap -->
            </div>
         <!-- END main-wrap -->   
      </section>
      <!-- END content-container -->
@endsection