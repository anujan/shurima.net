@layout('master')
@section('title')
Livestreams
@endsection
@section('assets')
@parent
<style type="text/css">
    .cat-2 {
        margin-bottom: 60px !important;
    }
</style>
@endsection
@section('description') Shurima.NET's stream page shows online livestreams of players along with information on the streamer such as the champion they're currently playing, runes and masteries being used, and their rank!@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">

        <div class="span12" id="content">

            <div id="post" class="post-single">
                <div class="content-outer">
                    <div class="content-inner">
                        <div id="gallery">
                            <ul class="gallery-navigation">
                                <li><a class="btn btn-primary active" id="all" href="#">All</a></li>
                                <li><a class="btn filter" id="cat-1" href="#">Show only ingame streams</a></li>
                            </ul>
                            <div class="separator"></div>
                            <div class="row-fluid" id="gallery-list">
                                <?php $count = 0; ?>
                                <ul>
                                @foreach ($streams as $stream)
                                <li id="stream_box" class="gallery-item@if ($stream->champion >= 0) cat-1 @else cat-2 @endif article-thumbnail" rel="gallery">
                                    <a title="{{$stream->ign}}" href="{{action('streams')}}/{{$stream->id}}/{{Str::slug($stream->ign)}}">
                                        <img alt="{{$stream->ign}}" src="{{$stream->thumbnail}}" style="height:130px;width:180px;"></a>
                                        <div id="stream_ign" class="caption">{{$stream->ign}}</div>
                                    <div class="stream_info">
                                        <div id="stream_league">{{$stream->tier}} {{$stream->division}}</div>
                                        <div id="stream_viewers">{{$stream->viewers}} Viewers</div>
                                        @if ($stream->champion >= 0)
                                        <div id="stream_champion"><?php	$champion = Champion::find($stream->champion);?>
                                            Currently playing: <a href="/champions/{{Str::lower($champion->name)}}" style="color: #AD235E">{{$champion->displayname}}</a>
                                        </div>
                                        @if ($stream->matchup != '')
                                        <div id="stream_champion"><a href="#{{$stream->channel}}Modal" data-toggle="modal" class="btn btn-primary">View Matchup</a></div>
                                        @endif
                                        @endif
                                    </div>
                                </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <p><em>Streamers playing on smurfs will not be shown as in-game.</em></p>
                <!-- content outer -->
            </div>

        </div>


    </section>
    <!-- End Main Content -->
</div>
@endsection

@section('modal')
@parent
@foreach ($streams as $stream)
@if ($stream->matchup != '')
<div id="{{$stream->channel}}Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="signInModalLabel">{{$stream->ign}}</h3>
    </div>
    <div class="modal-body" style="color:#000">
        <?php $spectate = unserialize($stream->matchup); ?>
        <div id="spectate" style="overflow:hidden;">
            <div class="span2">
                @if (isset($spectate['teamone']))
                    @foreach ($spectate['teamone'] as $teamone)
                    <?php
                    if (!isset($teamone['champion']))
                        continue; //It was a ban.
                    $t = Champion::find($teamone['champion']);
                    ?>
                    <p>
                        <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                        <strong style="font-size:18px;">{{$teamone['name']}}</strong>
                    </p>
                    @endforeach
                @endif
            </div>
            <div class="span1" style="margin-top:12%; text-align:center; font-size: 32px;">
                VS
            </div>
            <div class="span2">
                @if (isset($spectate['teamtwo']))
                    @foreach ($spectate['teamtwo'] as $teamtwo)
                    <?php
                    if (!isset($teamtwo['champion']))
                        continue; //It was a ban.
                    $t = Champion::find($teamtwo['champion']);
                    ?>
                    <p>
                        <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                        <strong style="font-size:18px;">{{$teamtwo['name']}}</strong>
                    </p>
                    @endforeach
                @endif
            </div>
            @if (false === true)
            <div id="bans" style="overflow:hidden;">
                <div class="span3" style="float: left;">
                    @foreach ($spectate['teamone']['bans'] as $ban)
                    <?php
                    $t = DB::connection('sqlite')->table('champions')->find($ban['championId']);
                    ?>
                    <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                    @endforeach
                </div>
                <div class="span3" style="font-size: 32px;">
                    <strong>BANS</strong>
                </div>
                <div class="span3" style="float: left;">
                    @foreach ($spectate['teamtwo']['bans'] as $ban)
                    <?php
                    $t = DB::connection('sqlite')->table('champions')->find($ban['championId']);
                    ?>
                    <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
@endif
@endforeach
@endsection