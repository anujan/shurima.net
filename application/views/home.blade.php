@layout('master')
@section('title')
Home
@endsection
@section('description')Shurima is a League of Legends fansite that provides tools focused on helping players improve at the game. It provides champion and item information as well as pro player videos and setups. It also features Elojournal which allows summoners to monitor their progress in ranked games. We also store tons of videos of streamers and have the best livestream system out there!@endsection

@section('content')
<div class="container hidden-phone" id="slideshow">
    <section id="slider" class="row">
        <div class="span8">
            <div class="camera_wrap camera_azure_skin" id="cameraslide">
                <div class="camera-slide-wrapper span8" data-src="{{asset('img/assets/slideshow/slide-1.png')}}">
                    <div class="camera_caption fadeFromLeft">
                        <em>We just launched our video archive!</em> Check out games of your favourite streamers and
                        champions!
                    </div>
                </div>
                <div class="camera-slide-wrapper span8" data-src="{{asset('img/assets/slideshow/slide-2.png')}}">
                    <div class="camera_caption fadeFromLeft">
                        Shurima.NET got a makeover! <em>Tweet @ShurimaNetwork with #ShurimaMakeover with your
                            opinions!</em>
                    </div>
                </div>
                <div class="camera-slide-wrapper span8" data-src="{{asset('img/assets/slideshow/slide-1.png')}}">
                    <div class="camera_caption fadeFromLeft">
                        Log all your ranked games with <em>EloJournal! </em>
                    </div>
                </div>
            </div>
            <!-- End Slider -->
        </div>
        <!-- End Span8 -->

        <!-- Begin Span4 Carousel Slider -->
        <div class="span4 carousel-widget visible-desktop" id="slider-widget">
            <div class="widget-header">
                <h3 class="widget-heading">Latest Videos</h3>
            </div>
            <div class="widget-content carousel slide" id="carousel-widget">
                <div class="carousel-inner">
                    <?php $videos = Video::where('pending', '=', '0')->order_by('updated_at', 'desc')->take(9)->get(); ?>
                    @for ($i = 0; $i < 3; $i++)
                    <div class="slide-section item @if ($i == 0)active@endif">
                        @for ($j=0; $j<3; $j++)
                        <?php if (!isset($videos[(3 * $i) + $j])) continue;
                        else $video = $videos[(3 * $i) + $j]; ?>
                        <div class="separator"></div>
                        <div class="slide-item media">
                            <a class="pull-left media-thumbnail"
                               href="{{route('video')}}/{{$video->id}}/{{Str::slug($video->ign." playing as ".$video->champion_name)}}">
                            <img class="media-object" lsrc="{{$video->thumbnail}}" alt="thumbnail"/>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a
                                        href="{{route('video')}}/{{$video->id}}/{{Str::slug($video->ign." playing as ".$video->champion_name)}}">{{$video->ign}} as {{$video->champion_name}}</a></h4>

                                <p>{{$video->tier}} {{$video->division}} player, {{$video->ign}}, playing as
                                    {{$video->champion_name}} on {{$video->created_at}}
                                </p>
                            </div>
                        </div>
                        @endfor
                    </div>
                    @endfor
                </div>
                <!-- End Carousel Inner -->
            </div>
        </div>
        <!-- End Span 4 Carousel Slider -->
    </section>
</div>
<div role="main" class="container">
    <section class="row home" id="main-content">
        <!-- Sidebar Content -->
        <div class="span3 visible-desktop" id="left-sidebar">

            <div id="ads" class="sidebar-widget">
                <div class="sidebar-content ads-widget">
                    <ul>
                        <li class="sidebar-item">
                            {{Utilities::adsense('vertical')}}
                        </li>
                    </ul>
                </div>
            </div>
            <div class="sidebar-widget">
                <a class="twitter-timeline" href="https://twitter.com/ShurimaNetwork"
                   data-widget-id="314098688756682753">Tweets by @ShurimaNetwork</a>
                <script>!function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//platform.twitter.com/widgets.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, "script", "twitter-wjs");</script>
            </div>
        </div>

        <!-- End Sidebar -->

        <!-- Main Content -->
        <div class="span6" id="content">
            <div class="post-lists">
                @foreach ($posts as $post)
                <article class="latest-article">
                    <div class="article-content">
                        <div class="article-header">
                            <h3><a title="{{$post->title}}" href="{{action('blog@post')}}/{{$post->id}}">{{$post->title}}</a>
                            </h3>
                        </div>
                        <div class="article-excerpt">
                            <p>
                                {{Str::html_limit($post->content, 800)}}
                                <a title="read more" href="{{action('blog@post')}}/{{$post->id}}">continue
                                    reading &rarr;</a>
                            </p>
                        </div>
                    </div>
                </article>
                <div class="separator"></div>
                @endforeach
            </div>
        </div>
        <!-- End Main Content -->

        <!-- Sidebar Right -->
        <div class="span3" id="right-sidebar">
            @if (Auth::guest())
            <div class="sidebar-widget sidebar-block sidebar-color">
                <div class="sidebar-header">
                    <h4>Login Form</h4>
                </div>
                <div class="sidebar-content login-widget">
                    <form action="{{action('elojournal@login')}}" method="post">
                        <div class="input-prepend">
                            <span class="add-on "><i class="icon-user"></i></span>
                            <input type="text" name="username" placeholder="username" class="span2">
                        </div>
                        <div class="input-prepend">
                            <span class="add-on "><i class="icon-lock"></i></span>
                            <input type="password" name="password" placeholder="password" class="span2">
                        </div>
                        <label class="checkbox">
                            <input type="checkbox">
                            Remember me
                        </label>

                        <div class="controls">
                            <button type="submit" class="btn signin">Sign In</button>
                            <button class="btn signup" href="{{action('elojournal@register')}}">Sign up</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            <div id="latest-reviews" class="sidebar-widget">
                <div class="sidebar-header">
                    <h4><a href="{{action("streams")}}" title="Livestreams">Livestreams</a></h4>

                    <div class="separator"></div>
                </div>
                <div class="sidebar-content post-widget">
                    <ul>
                        @foreach (Utilities::getLiveStreams(7) as $s)
                        <li class="sidebar-item">
                            <a href="{{action('streams')}}/{{$s->id}}/{{Str::slug($s->ign)}}" class="image-polaroid"
                               title="{{$s->ign}}">
                                <img lsrc="{{str_replace("320x200", "60x60",$s->thumbnail)}}" alt="{{$s->ign}}" />
                            </a>
                            <h5><a href="{{action('streams')}}/{{$s->id}}/{{Str::slug($s->ign)}}">{{$s->ign}}
                                    @if ($s->champion >=0)
                                    - {{Champion::find($s->champion)->displayname}}
                                    @endif</a></h5>
                            <span class="post-date"><em>{{$s->viewers}} Viewers</em></span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(function () {
        $.each(document.images, function () {
            var this_image = this;
            var src = $(this_image).attr('src') || '';
            if (!src.length > 0) {
                var lsrc = $(this_image).attr('lsrc') || '';
                if (lsrc.length > 0) {
                    var img = new Image();
                    img.src = lsrc;
                    $(img).load(function () {
                        this_image.src = this.src;
                    });
                }
            }
        });
    });
</script>
@endsection