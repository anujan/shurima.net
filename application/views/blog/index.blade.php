@layout('master')
@section('title')
Blog
@endsection
@section('description') Shurima is a League of Legends fansite that provides tools focused on helping players improve at the game. It provides champion and item information as well as pro player videos and setups. It also features Elojournal which allows summoners to monitor their progress in ranked games. @endsection
@section('content')
<div role="main" class="container">
<!-- Main Content -->{{Utilities::adsense()}}
<section class="row" id="main-content">

<div class="span8" id="content">
    <ul class="breadcrumb">
        <li class="typ-home"><a href="{{route('home')}}">Home</a> <span class="divider">»</span></li>
        <li class="active typ-pin"><a href="{{action('blog')}}">Blog</a> <span class="divider">»</span></li>
    </ul>
    <div id="post" class="post-lists">
        <div class="content-outer">
            <div class="content-inner">
                @foreach ($posts->results as $post)
                <article class="latest-article">
                    <div class="article-content">
                        <div class="article-header">
                            <h3><a title="{{$post->title}}" href="{{action('blog@post')}}/{{$post->id}}">{{$post->title}}</a></h3>
                        </div>
                        <div class="article-excerpt">
                            <p>
                                {{Str::html_limit($post->content, 850)}}
                                <a title="read more" href="{{action('blog@post')}}/{{$post->id}}">continue reading &rarr;</a>
                            </p>
                        </div>
                    </div>
                </article>
                <div class="separator"></div>
                @endforeach
            </div>
            <!-- content inner -->
        </div>
        <!-- content outer -->
    </div>
    <div class="pagination center">
        {{$posts->links()}}
    </div>
</div>

<div class="span4" id="right-sidebar">
    @if (Auth::guest())
    <div class="sidebar-widget sidebar-block sidebar-color">
        <div class="sidebar-header">
            <h4>Login Form</h4>
        </div>
        <div class="sidebar-content login-widget">
            <form action="{{action('elojournal@login')}}" method="post">
                <div class="input-prepend">
                    <span class="add-on "><i class="icon-user"></i></span>
                    <input type="text" name="username" placeholder="username" class="span2">
                </div>
                <div class="input-prepend">
                    <span class="add-on "><i class="icon-lock"></i></span>
                    <input type="password" name="password" placeholder="password" class="span2">
                </div>
                <label class="checkbox">
                    <input type="checkbox">
                    Remember me
                </label>
                <div class="controls">
                    <button type="submit" class="btn signin">Sign In</button>
                    <button class="btn signup" href="{{action('elojournal@register')}}">Sign up</button>
                </div>
            </form>
        </div>
    </div>
    @endif
</div>
</section>
</div>
@endsection