@layout('master')
@section('title')
{{$post->title}} - Blog
@endsection
@section('description')
{{strip_tags($post->content)}}
@endsection
@section('content')
<div role="main" class="container">
<!-- Main Content --> {{Utilities::adsense()}}
<section class="row" id="main-content">

<div class="span8" id="content">

    <div id="post" class="post-single">
        <div class="content-outer">
            <div class="content-inner">
                <article>
                    <div class="article-header">
                        <h1 class="title">{{$post->title}}</h1>
                        <ul class="article-info">
                            <li><span class="icon ent-user"></span><a href="#">{{$post->user->username}}</a></li>
                            <li><span class="icon ent-clock"></span>{{$post->created_at}}</li>
                            </li>
                        </ul>
                        <div class="separator"></div>
                    </div>
                    <div class="article-content">
                        {{$post->content}}
                    </div>

                    <div class="separator"></div>
                    <div class="author">
                        <div class="author-avatar image-polaroid">
                            <img alt="thumbnail" src="{{$post->user->get_gravatar()}}">
                        </div>
                        <div class="author-profile">
                            <strong>About The Author</strong>

                            <p>{{$post->user->about}}
                            </p>
                        </div>
                    </div>
                    <div class="separator"></div>
                    <div class="comments">
                        <h3>{{count($post->comments)}} Comments</h3>

                        <div class="separator"></div>
                        <ul class="commentlists">
                            @foreach ($post->comments as $comment)
                            <li>
                                <div class="comment-author image-polaroid">
                                    <img alt="avatar" src="{{$comment->user->get_gravatar()}}">
                                </div>
                                <div class="comment-body">
                                    <div class="comment-meta">
                                        <span class="meta-name"><a href="#">{{$comment->user->username}}</a></span>
                                        <span class="meta-date">{{$comment->created_at}}</span>
                                    </div>
                                    <p>
                                        {{htmlspecialchars($comment->content)}}
                                    </p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="respond">
                        <h3>Leave a Comment</h3>

                        <div class="separator"></div>

                        <form id="commentform" method="POST">
                            <div class="comment-text">
                                <label for="text-comment" class="control-label">Comment</label>

                                <div class="control">
                                    <div class="input-border">
                                        <textarea placeholder="Your Comment" id="text-comment" rows="10"
                                                  name="comment"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-submit">
                                <button type="submit" class="btn">submit</button>
                            </div>
                        </form>
                    </div>

                </article>


            </div>

            <!-- content inner -->
        </div>
        <!-- content outer -->
    </div>

</div>


<!-- Sidebar Right -->
<div class="span4" id="right-sidebar">
<!-- Sidebar Block Widget -->
@if (Auth::guest())<div class="sidebar-widget sidebar-block sidebar-color">
        <div class="sidebar-header">
            <h4>Login Form</h4>
        </div>
        <div class="sidebar-content login-widget">
            <form action="{{action('elojournal@login')}}" method="post">
                <div class="input-prepend">
                    <span class="add-on "><i class="icon-user"></i></span>
                    <input type="text" name="username" placeholder="username" class="span2">
                </div>
                <div class="input-prepend">
                    <span class="add-on "><i class="icon-lock"></i></span>
                    <input type="password" name="password" placeholder="password" class="span2">
                </div>
                <label class="checkbox">
                    <input type="checkbox">
                    Remember me
                </label>
                <div class="controls">
                    <button type="submit" class="btn signin">Sign In</button>
                    <button class="btn signup" href="{{action('elojournal@register')}}">Sign up</button>
                </div>
            </form>
        </div>
    </div>
@endif
</div>
<!-- Sidebar End -->
</section>
<!-- End Main Content -->
</div>
@endsection