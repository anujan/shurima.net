@layout('master')

@section('title')
Create Post - Blog
@endsection

@section('assets')
@parent
<link rel="stylesheet" href="{{asset('redactor/redactor.css')}}" />
<script src="{{asset('redactor/redactor.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(
        function()
        {
            $('#editor').redactor();
        }
    );
</script>
<style>
    label {
        color: #000000;
    }
</style>
@endsection

@section('content')
<div role="main" class="container">
    <!-- Main Content -->
    <section class="row" id="main-content">
        <div class="span10" id="content">
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                            @if (isset($error))
                            <div class="alert alert-error">
                                <strong>Error!</strong> {{$error}}
                            </div>
                            @endif
                            <br/>
                            {{Form::open()}}
                            {{Form::label('title', 'Title')}}
                            {{Form::text('title')}}
                            {{Form::textarea('content', '', array('autofocus' => 'autofocus', 'style' => 'width: 100%;', 'id' => 'editor'))}}
                            <br/>
                            {{Form::submit("Submit")}}
                            {{Form::close()}}
                    </div>
                </div>
            </div>
    </section>
</div>
<script type="text/javascript">
    var formEditor = new SnapEditor.Form("editor", {
    });
</script>
@endsection