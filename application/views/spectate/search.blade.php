@layout('master')

@section('title')
{{Input::get('summoner')}} - Spectate
@endsection
@section('description') Shurima is a League of Legends fansite that provides tools focused on helping players improve at the game. Spectate allows you to spectate summoners from any region without logging into the League of Legends client. @endsection

@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span9" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="typ-pin"><a href="{{action('spectate')}}">Spectate</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin">{{Input::get('summoner')}}</li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                        @if (isset($error))
                        <div class="alert alert-error">
                                <p style="font-size:12px;"> <strong>Error!</strong> {{$error}} </p>
                        </div>
                        @endif
                        <div id="spectate" style="overflow:hidden;">
                                <div class="span3">
                                        @foreach ($spectate['teamone'] as $teamone)
                                        <?php
                                        if (!isset($teamone['champion']))
                                                continue; //It was a ban.
                                        $t = DB::connection('sqlite')->table('champions')->find($teamone['champion']);
                                        ?>
                                        <p>
                                                <a href="http://shurima.net/champions/{{$t->displayname}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                                                <strong style="font-size:18px;">{{$teamone['name']}}</strong>
                                        </p>
                                        @endforeach
                                </div>
                                <div class="span2" style="margin-top:12%; text-align:center; font-size: 32px;">
                                        VS
                                        <br><br>
                                        <a target="_self" class="btn btn-primary" href="{{$spectate['lrf']}}"> SPECTATE (LOLREPLAY) </a>
                                        <br><br>
                                        <a target="_self" class="btn btn-inverse" href="#spectateModal" data-toggle="modal"> SPECTATE (CMD PROMPT) </a>
                                </div>
                                <div class="span3">
                                        @foreach ($spectate['teamtwo'] as $teamtwo)
                                        <?php
                                        if (!isset($teamtwo['champion']))
                                                continue; //It was a ban.
                                        $t = DB::connection('sqlite')->table('champions')->find($teamtwo['champion']);
                                        ?>
                                        <p>
                                                <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                                                <strong style="font-size:18px;">{{$teamtwo['name']}}</strong>
                                        </p>
                                        @endforeach
                                </div>
                                @if (false === true)
                                <div id="bans" style="overflow:hidden;">
                                        <div class="span3" style="float: left;">
                                                @foreach ($spectate['teamone']['bans'] as $ban)
                                                <?php
                                                $t = DB::connection('sqlite')->table('champions')->find($ban['championId']);
                                                ?>
                                                <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                                                @endforeach
                                        </div>
                                        <div class="span3" style="font-size: 32px;">
                                                <strong>BANS</strong>
                                        </div>
                                        <div class="span3" style="float: left;">
                                                @foreach ($spectate['teamtwo']['bans'] as $ban)
                                                <?php
                                                $t = DB::connection('sqlite')->table('champions')->find($ban['championId']);
                                                ?>
                                                <a href="http://shurima.net/champions/{{$t->name}}" alt="{{$t->displayname}}"><img width="50px" height="50px" src="{{asset(Utilities::$champion_assets_path . $t->iconpath)}}"></a>
                                                @endforeach
                                        </div>
                                </div>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<div class="modal hide in" id="spectateModal" aria-hidden="false">
    <div id="spectateModal" class="modal-body">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <p style="color:#000">Copy and paste the following command into the Command Prompt.</p>
        <textarea readonly="readonly" id="cmdPromptCommand" style="cursor:default;width:510px" cols="1" rows="8">{{$spectate['cmd']}}</textarea>
        <p style="color:#BBBBBB;font-size:11px">Please note: this command will only work if League of Legends is installed in the default location.</p>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn">Close</button>
    </div>
</div>
@endsection