@layout('master')

@section('title')
Spectate
@endsection
@section('description') Shurima is a League of Legends fansite that provides tools focused on helping players improve at the game. Spectate allows you to spectate summoners from any region without logging into the League of Legends client. @endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">
        <div class="span8" id="content">
            <ul class="breadcrumb">
                <li class="typ-home"><a href="/">Home</a> <span class="divider">&raquo;</span></li>
                <li class="active typ-pin"><a href="{{action('spectate')}}">Spectate</a></li>
            </ul>
            <div id="post" class="post-lists">
                <div class="content-outer">
                    <div class="content-inner">
                @if (isset($error))
                <div class="alert alert-error">
                        <strong>Error!</strong> {{$error}}
                </div>
                @endif
                <div id="spectate">
                        {{Form::open('spectate/search', 'GET')}}
                        {{Form::label('summoner', 'Summoner Name')}}
                        {{Form::text('summoner')}}
                        {{Form::label('region', 'Region')}}
                        {{Form::select('region', array('na' => 'NA', 'euw' => 'EUW', 'eune' => 'EUNE', 'br' => 'BR', 'tr' => 'TR', 'tw' => 'Garena TW', 'sg' => 'Garena SG/MY/SEA'))}}
                        <p><p></p>{{Form::submit("Spectate", array("class" => "btn"))}}</p>
                        {{Form::close()}}
                </div>
                    </div>
                </div>
            </div>
    </section>
</div>
@endsection