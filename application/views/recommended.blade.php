@layout('master')

@section('title')
Recommended Item Changer
@endsection
@section('description')Shurima.NET's recommended item changer allows you to change the recommended items for any champion in League of Legends.@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->
    <section class="row" id="main-content">
        <div class="span12" id="content">
            <div id="post" class="post-single">
                <div class="content-outer">
                    <div class="content-inner">
                        {{Utilities::adsense()}}
                        {{Form::open('/recommended')}}
                        {{Form::label('champion', 'Champion')}}
                        {{Form::select('champion', Utilities::getChampions())}}
                        {{Form::label('map', 'Map')}}
                        {{Form::select('map', array('any' => 'Any', '1' => "Summoner's Rift", "8" => "Crystal Scar", "10" => "Twisted Treeline", "3" => "Proving Grounds"))}}
                        <?php
                        $types = array('starting', 'essential', 'offensive', 'defensive');
                        ?>
                        @foreach ($types as $type)
                        {{Form::label($type."[]", Str::title($type)." Items")}}
                        {{Form::select($type."[]", Utilities::getItems(), null, array('multiple' => 'multiple', 'style' => 'height: 200px;'))}}
                        @endforeach
                        <br/>
                        <br/>
                        {{Form::submit("Submit")}}
                        <p>Once you're ready, hit submit and read the <b>README</b> file in the .zip you download.</p>
                        {{Form::close()}}
                    </div>
                </div>
    </section>
    <!-- End Main Content -->
</div>
@endsection