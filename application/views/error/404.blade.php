@layout('../master')
@section('title') Error 404 - Not Found @endsection
@section('content')
<div class="container" role="main">
    <!-- Main Content -->
    <section id="main-content" class="row">

        <div id="content" class="span12">

            <div class="post-single" id="post">
                <div class="content-outer">
                    <div class="content-inner">
                        <div class="error404">
                            <h4 class="">404 Error Page Not Found</h4>
                            <p class="">Sorry we couldn't find the page you're searching for</p>
                        </div>
                        <div class="error404">
                            <h4 class="style2">404 Error Page Not Found</h4>
                            <p class="">Sorry we couldn't find the page you're searching for</p>
                        </div>

                        <div class="error404">
                            <h4 class="style3">404 Error Page Not Found</h4>
                            <p class="">Sorry we couldn't find the page you're searching for</p>
                        </div>

                    </div>

                    <!-- content inner -->
                </div>
                <!-- content outer -->
            </div>

        </div>


    </section>
    <!-- End Main Content -->

</div>
@endsection
