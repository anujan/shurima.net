@layout('master')

@section('title')
Raffle
@endsection
@section('content')
<section class="small_banner">
    <div class="center-wrap">
        <p class="page-banner-heading">Shurima.net Giveaway!</p>
        <div class="breadcrumbs">
            <a href="/">Home</a> &rarr; <span class="current_crumb">Raffle</span>
        </div>
    </div>
    <div class="shadow top"></div>
    <div class="shadow bottom"></div>
    <div class="tt-overlay"></div>
</section>
<section class="clearfix" id="content-container">
    <div class="clearfix" id="main-wrap">
        <div class="full_width">
            {{Utilities::adsense()}}
            <p></p>
            <h2>We at Shurima our giving away $100 of RP codes to our users on March 24th! <br>There will be 10 $10 RP codes up for grabs, you get entered to win by clicking on the links and doing the following!</h2>
            <p></p>
            <p></p>
            <ul style="font-size: 20px"> 
                <li><a class="facebook" href="http://www.facebook.com/ShurimaNetwork">Facebook</a></li><ul>
                <li><a class="facebook" href="http://www.facebook.com/ShurimaNetwork">Like our Facebook Page (2 card raffle)</a></li>
                <li><a class="facebook" href="https://www.facebook.com/ShurimaNetwork/posts/374199946027539">Share our Facebook giveaway post (2 card raffle)</a></li></ul>
                <li>Twitter</li>
                <ul>
                <li><a class="twitter" href="http://www.twitter.com/ShurimaNetwork">Follow us on Twitter (2 card raffle)</a></li>
                <li><a class="twitter" href="https://twitter.com/ShurimaNetwork/status/306841460857245697">Retweet our giveaway tweet (2 card raffle)</a></li>
                </ul>
                <li><a href="http://shurima.net/elojournal">EloJournal</a></li><ul>
                <li>Log 5 or more games in total on Elojournal (1 card raffle)</li>
                <li>Log 3 or more games since February 25th, 2013 (1 card raffle)</li></ul>
            </ul>
            <h5>In the event that one of our social media pages gets more than 1k followers, we'll double the amount of prizes we give! (20 cards of $10 RP instead)</h5>
            <p></p>
            {{Utilities::adsense()}}
        </div>
    </div>
</section>
@endsection