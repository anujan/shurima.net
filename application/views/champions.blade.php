@layout('master')
@section('title')
Champions
@endsection
@section('description')
ShurimaDB is a information database of all items and champions in League of Legends. Every champion in the game along with stats, abilities, lore, and skins are shown and updated regularly!
@endsection
@section('content')
<div role="main" class="container">
    <!-- Main Content -->{{Utilities::adsense()}}
    <section class="row" id="main-content">

        <div class="span12" id="content">

            <div id="post" class="post-single">
                <div class="content-outer">
                    <div class="content-inner">
                        <div id="gallery">
                            <ul class="gallery-navigation">
                                <li><a class="btn btn-primary active" id="all" href="#">All</a></li>
                                @for ($i = 65; $i < 91; $i++)
                                <li><a href="#" class="btn filter" id="cat-{{$i}}" title="View all Champions filed under {{chr($i)}}">{{chr($i)}}</a></li>
                                @endfor
                            </ul>
                            <div class="separator"></div>
                            <div class="row-fluid" id="gallery-list">
                                <?php $count = 0; ?>
                                <ul>
                                     {{Utilities::getChampionList()}}
                                </ul>
                            </div>
                        </div>

                    </div>

                    <!-- content inner -->
                </div>
                <!-- content outer -->
            </div>

        </div>


    </section>
    <!-- End Main Content -->

</div>
@endsection