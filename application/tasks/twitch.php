<?php
class Twitch_Task {

    public function run($arguments)
    {
        gc_enable();
        Utilities::updateStreams();
        VideoArchive::updateVideos();
        Utilities::updateLeagues();
        gc_collect_cycles();
        echo 'Memory Usage:'.memory_get_usage();
    }
}
?>