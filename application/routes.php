<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
  | breeze to setup your application using Laravel's RESTful routing and it
  | is perfectly suited for building large applications and simple APIs.
  |
  | Let's respond to a simple GET request to http://example.com/hello:
  |
  |		Route::get('hello', function()
  |		{
  |			return 'Hello World!';
  |		});
  |
  | You can even respond to more than one URI:
  |
  |		Route::post(array('hello', 'world'), function()
  |		{
  |			return 'Hello World!';
  |		});
  |
  | It's easy to allow URI wildcards using (:num) or (:any):
  |
  |		Route::put('hello/(:any)', function($name)
  |		{
  |			return "Welcome, $name.";
  |		});
  |
 */

Route::get('streams', array('as' => 'streams', 'do' => function () {
        $streams = Utilities::getLiveStreams();
        return View::make('streams')->with('streams', $streams);
    })
);

Route::get('streams/(:num)', array('as' => 'streams', 'do' => function ($id) {
        $stream = Stream::find($id);
        return Redirect::to('streams/' . $id . '/' . Str::slug($stream->title), 301);
    })
);

Route::get('streams/(:num)/(:any)', array('as' => 'streams', 'do' => function ($id, $slug) {
        $stream = Stream::find($id);
        if (!$stream->acctid || !$stream->masterypage || !$stream->runepage) {
            if ($stream->acctid == 0) {
                $stream->acctid = Utilities::getAcctIdByName($stream->ign, $stream->region);
            }
            if ($stream->runepage == '') {
                $stream->runepage = serialize(Utilities::getCurrentRunepage($stream->region, $stream->acctid));
            }
            if ($stream->masterypage == '') {
                $stream->masterypage = serialize(Utilities::getCurrentMasteryPage($stream->region, $stream->summonerid));
            }
            $stream->save();
        }
        return View::make('stream')->with('stream', $stream);
    })
);

Route::get('videos', array('as' => 'videos', 'do' => function () {
        $videos = Video::where('pending', '=', '0')->order_by('id', 'desc')->paginate(25);
        return View::make('videos.list')->with('videos', $videos);
    })
);

Route::get('videos/(:num)', array('as' => 'video', 'do' => function ($id) {
        $video = Video::find($id);
        if ($video === null || $video->pending)
            return Response::error('404');
        return Redirect::to('videos/' . $id . '/' . Str::slug($video->ign . " playing " . Champion::find($video->champion)->displayname), 301);
    })
);

Route::get('videos/(:num)/(:any)', array('as' => 'video', 'do' => function ($id, $slug) {
        $video = Video::find($id);
        if ($video === null || $video->pending)
            return Response::error('500');
        return View::make('videos.play')->with('video', $video);
    })
);

Route::post('videos/search', array('as' => 'video_search', 'do' => function () {
    $str = explode(" ", Input::get('query'));
    $videos = Video::where('pending', '=', '0');
    foreach ($str as $q) {
        $videos->where(function ($query) use ($q) {
            $q = '%' . $q . '%';
            $query->or_where('channel', 'LIKE', $q);
            $query->or_where('ign', 'LIKE', $q);
            $query->or_where('champion_name', 'LIKE', $q);
            $query->or_where('tier', 'LIKE', $q);
        });
    }
    $videos = $videos->order_by('created_at', 'desc')->paginate(24);
    return View::make('videos.list')->with('videos', $videos);

}));

Route::controller('elojournal');
Route::controller('spectate');
Route::controller('blog');

Route::get('/', array('as' => 'home', 'do' => function () {
        $posts = Post::order_by('id', 'desc')->take(5)->get();
        return View::make('home')->with('posts', $posts);
    })
);
Route::get('contact', array('as' => 'contact', 'do' => function () {
    return View::make('contact');
}));

Route::get('raffle', array('as' => 'raffle', 'do' => function () {
    return View::make('raffle');
}));
Route::post('contact', array('before' => 'csrf', 'do' => function () {
    $input = Input::all();
    $rules = array(
        'email' => 'required|email',
        'name' => 'required',
        'comments' => 'required',
        'verify' => 'required|laracaptcha'
    );
    $v = Validator::make($input, $rules);
    if ($v->fails()) {
        return View::make('contact')->with('error', $v->errors->first())->with_input();
    } else {
        Bundle::start('swiftmailer');
        $mailer = IoC::resolve('mailer');
        $message = Swift_Message::newInstance('Contact Us Submission')
            ->setFrom(array(Input::get('email') => Input::get('name')))
            ->setTo(array('admin@shurima.net' => 'Shurima'))
            ->setBody(Input::get('comments'), 'text/html');
        $mailer->send($message);
        return Redirect::to_route('home')->with('success', "Your email was sent successfully! We'll get back to you ASAP!");
    }
}));

Route::get('database', array('as' => 'database', 'do' => function () {
    return View::make('database');
}));

Route::get('items/(:any)', array('as' => 'item', 'do' => function ($item) {
    $name = str_replace("-", "%", $item);
    $name = str_replace("(", "%", $name);
    $name = str_replace(")", "%", $name);
    $item = Item::where("name", "LIKE", $name)->first();
    if ($item === null) {
        return Response::error('404');
    }
    return View::make('item')->with('item', $item);
}));

Route::get('items', array('as' => 'items', 'do' => function () {
    return View::make('items');
}));

Route::get('champions/(:any)', array('as' => 'champion', 'do' => function ($champion) {
    $champion = Champion::where('name', 'like', $champion)->first();
    if ($champion == null) {
        return Response::error('404');
    }
    return View::make('champion')->with('champion', $champion);
}));

Route::get('champions', array('as' => 'champions', 'do' => function () {
    return View::make('champions');
}));

Route::get('tools/(:any)', array('as' => 'tools', 'do' => function ($tool) {
    $tools = array('pickacard', 'recommended', 'newrecommended');
    if (in_array($tool, $tools)) {
        if ($tool == 'newrecommended')
            $tool = 'recommended';
        return View::make($tool);
    } else {
        return Response::error('404');
    }
}));

Route::post('recommended', array('as' => 'recommendeditems', 'do' => function () {
    $input = Input::all();
    $rules = array('champion' => 'required',
        'map' => 'required',
        'starting' => 'required',
        'essential' => 'required',
        'offensive' => 'required',
        'defensive' => 'required'
    );
    $v = Validator::make($input, $rules);
    if ($v->fails()) {
        return View::make('recommended')->with('error', $v->errors->first())->with_input();
    } else {
        Utilities::recommendedItems();
        return;
    }
}));

Route::get('team', array('as' => 'team', 'do' => function () {
    return View::make('team');
}));

Route::get('vonwurfel', function () {
    $stream = Stream::find(323);
    return View::make('stream')->with('stream', $stream);
});

Route::get('mrquackers', function () {
    $stream = Stream::find(324);
    return View::make('stream')->with('stream', $stream);
});
Route::get('forget/(:any)', function ($key) {
        Cache::forget($key);
        return Redirect::to('home');
    }
);

Route::get('differences', function () {
        $liveChamps = Champion::get();
        $liveItems = Item::get();
        $liveAbilities = Ability::get();
        $champion_differences = array();
        foreach ($liveChamps as $champ) {
            Config::set('database.default', 'pbe');
            $pbeChamp = Champion::find($champ->id);
            $difference = array_diff($champ->to_array(), $pbeChamp->to_array());
            if (!empty($difference)) {
                $champion_differences[$champ->displayname] = $difference;
            }
        }
        $ability_differences = array();
        foreach ($liveAbilities as $ability) {
            $pbeAbility = Ability::where('name', '=', $ability->name)->first();
            if ($pbeAbility == null) continue;
            $difference = array_diff($ability->to_array(), $pbeAbility->to_array());
            $difference2 = array_diff($pbeAbility->to_array(), $ability->to_array());
            if (isset($difference['id']))
                unset($difference['id']);
            if (isset($difference2['id']))
                unset($difference2['id']);
            if (!empty($difference)) {
                $ability_differences['new'][$ability->name] = $difference;
                $ability_differences['old'][$ability->name] = $difference2;
            }
        }
        print_r($champion_differences);
        print_r($ability_differences);
        echo 'done';
        //TODO finish this
    }
);

Route::get('api/(:any)/(:any)/(:all)', function ($region, $method, $args) {
    $args = urldecode($args);
    echo Utilities::makeShurimaAPIRequest($region, $method, $args);
    exit(1);
});

Route::get('addallstreams', function () {
    ignore_user_abort();
    set_time_limit(10000000);
    Utilities::updateStreams();
    Utilities::updateLeagues();
    VideoArchive::updateVideos();
    echo memory_get_usage()."\r\n";
    exit(1);
});

Route::get('addstreams', function () {
    Utilities::updateStreams();
});

Route::get('runepage/(:num)', function ($id) {
    $stream = Stream::find($id);
    return View::make('runepage')->with('runepage', unserialize($stream->runepage));
});

Route::get('mastery/(:num)', function ($id) {
    $stream = Stream::find($id);
    return View::make('masterypage')->with('page', unserialize($stream->masterypage));
});

Route::get('adcarmor', function () {
    $adcs = Champion::where('tags', 'LIKE', '%ranged%')->where('tags', 'LIKE', '%carry%')->order_by('armorbasae', 'desc')->get();
    foreach ($adcs as $adc) {
        echo $adc->displayname . "  -  " . $adc->armorbase . "  -  " . $adc->armorlevel . "<br>";
    }
});
/*
  |--------------------------------------------------------------------------
  | Application 404 & 500 Error Handlers
  |--------------------------------------------------------------------------
  |
  | To centralize and simplify 404 handling, Laravel uses an awesome event
  | system to retrieve the response. Feel free to modify this function to
  | your tastes and the needs of your application.
  |
  | Similarly, we use an event to handle the display of 500 level errors
  | within the application. These errors are fired when there is an
  | uncaught exception thrown in the application.
  |
 */

Event::listen('404', function () {
    return Response::error('404');
});

Event::listen('500', function () {
    return Response::error('500');
});

/*
  |--------------------------------------------------------------------------
  | Route Filters
  |--------------------------------------------------------------------------
  |
  | Filters provide a convenient method for attaching functionality to your
  | routes. The built-in before and after filters are called before and
  | after every request to your application, and you may even create
  | other filters that can be attached to individual routes.
  |
  | Let's walk through an example...
  |
  | First, define a filter:
  |
  |		Route::filter('filter', function()
  |		{
  |			return 'Filtered!';
  |		});
  |
  | Next, attach the filter to a route:
  |
  |		Router::register('GET /', array('before' => 'filter', function()
  |		{
  |			return 'Hello World!';
  |		}));
  |
 */

Route::filter('before', function () {
    if (URI::is('elojournal/*') || URI::is('elojournal')) {
        Config::set('database.default', 'shurima');
    } else if (strpos(URL::full(), "pbe.")) {
        Laravel\Config::set('database.default', 'pbe');
    }
});

Route::filter('after', function ($response) {
});

Route::filter('csrf', function () {
    if (Request::forged())
        return Response::error('500');
});

Route::filter('auth', function () {
    if (Auth::guest())
        return Redirect::to('login');
});