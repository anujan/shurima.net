<?php

class Game extends Eloquent {

        public static $timestamp = false;
        public static $table = 'games';

        public function user() {
                return $this->belongs_to('User', 'user_id');
        }
}

?>