<?php
class VideoArchive
{
    public static function addGame($stream)
    {
        $video = new Video;
        $video->stream_id = $stream->id;
        $video->channel = $stream->channel;
        $video->ign = $stream->ign;
        $video->url = '';
        $video->pending = 1;
        $video->time = time();
        $video->tier = $stream->tier;
        $video->division = $stream->division;
        $video->champion = $stream->champion;
        $video->champion_name = Champion::find($stream->champion)->displayname;
        $video->matchup = $stream->matchup;
        $video->runepage = $stream->runepage;
        $video->masterypage = $stream->masterypage;
        $video->save();
    }

    public static function updateVideos()
    {
        if (Cache::has('update_videos'))
            return;
        Cache::put('update_videos', 60, 60);
        $arr = array();
        $videos = Video::where("pending", "=", "1")->order_by('channel')->get();
        $current_channel = '';
        foreach ($videos as $video) {
            if (isset($arr[$video->channel])) {
                if ($current_channel != '' && $current_channel != $video->channel) {
                    unset($arr[$current_channel]);
                }
                $current_channel = $video->channel;
                $data = $arr[$video->channel];
            } else {
                $data = Utilities::readURL("https://api.twitch.tv/kraken/channels/{$video->channel}/videos?limit=10&broadcasts=true");
                $data = json_decode($data, true);
                $arr[$video->channel] = $data;
            }
            foreach ($data['videos'] as $vod) {
                $recorded_at = strtotime($vod['recorded_at']);
                $time = $video->time - $recorded_at;
                if ($time <= $vod['length'] && $time >= 0) {
                    $video->pending = 0;
                    $video->url = $vod['url'];
                    if (is_array($vod['preview']))
                        $vod['preview'] = $vod['preview'][0];
                    $video->thumbnail = $vod['preview'];
                    $video->time = $time;
                    $video->save();
                }
            }
        }
    }
}