<?php

class Item extends Eloquent {
    public static $table = 'items';
    public static $timestamp = false;
    
    public function buildsInto() {
        return $this->has_many('Recipe', 'recipeItemId');
    }
    
    public function buildsFrom() {
        return $this->has_many('Recipe', 'buildsToItemId');
    }
    
    public function getTotalPrice() {
        $price = $this->price;
        foreach ($this->buildsFrom()->get() as $r) {
            if (isset($r->itemFrom()->first()->price))
                $price += $r->itemFrom()->first()->price;
        }
        return $price;
    }
}
?>
