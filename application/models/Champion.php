<?php

class Champion extends Eloquent {
    public static $timestamp = false;
    public static $table = 'champions';
    public static $connection = 'sqlite';
    
    public function abilities() {
        return $this->has_many('Ability', 'championid');
    }
    
    public function skins() {
        return DB::table('championskins')->where('championid', '=', $this->id)->get();
    }
}
?>