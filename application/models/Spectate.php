<?php

class Spectate {

    public static function find($summoner, $region) {
        $version = "1.0.0.152";
        $game_version = '0.0.0.223';
        $client_version = '0.0.0.253';
        $summoner = str_replace(" ", "", $summoner);
        $summoner = urlencode($summoner);
        $json = Utilities::makeShurimaAPIRequest($region, 'spectator', $summoner);
        $data = json_decode($json, true);
        $success = is_array($data);
        if ($success) {
            $info = array();
            $platform = '';
            foreach ($data['game']['teamOne']['array'] as $teamone) {
                $key = $teamone['summonerInternalName'];
                $info['teamone'][$key]['accountId'] = $teamone['accountId'];
                $info['teamone'][$key]['summonerId'] = $teamone['summonerId'];
                $info['teamone'][$key]['name'] = $teamone['summonerName'];
                $info['teamone'][$key]['pickTurn'] = $teamone['pickTurn'];
                if (isset($teamone['originalPlatformId']))
                    $platform = $teamone['originalPlatformId'];
            }
            foreach ($data['game']['teamTwo']['array'] as $teamtwo) {
                $key = $teamtwo['summonerInternalName'];
                $info['teamtwo'][$key]['accountId'] = $teamone['accountId'];
                $info['teamtwo'][$key]['summonerId'] = $teamone['summonerId'];
                $info['teamtwo'][$key]['name'] = $teamtwo['summonerName'];
                $info['teamtwo'][$key]['pickTurn'] = $teamtwo['pickTurn'];
                if (isset($teamtwo['originalPlatformId']))
                    $platform = $teamtwo['originalPlatformId'];
            }
            foreach ($data['game']['bannedChampions']['array'] as $bans) {
                if ($bans['teamId'] == 100)
                    $info['teamone']['bans'][] = $bans;
                else
                    $info['teamtwo']['bans'][] = $bans;
            }
            foreach ($data['game']['playerChampionSelections']['array'] as $picks) {
                $key = $picks['summonerInternalName'];
                if (array_key_exists($key, $info['teamone'])) {
                    $info['teamone'][$key]['champion'] = $picks['championId'];
                } else {
                    $info['teamtwo'][$key]['champion'] = $picks['championId'];
                }
            }
            $info['spectate'] = $data['playerCredentials'];
            $info['gameId'] = $info['spectate']['gameId'];
            $lrf = "lrf://spectator {$info['spectate']['observerServerIp']}:{$info['spectate']['observerServerPort']} {$info['spectate']['observerEncryptionKey']} {$info['spectate']['gameId']} {$platform} {$version}";
            $info['lrf'] = $lrf;
            if ($platform == 'NA')
                $platform = 'NA1';
            $info['cmd'] = "cd \"C:/Riot Games/League of Legends/RADS/solutions/lol_game_client_sln/releases/$game_version/deploy/\" & \"League of Legends.exe\" \"8394\" \"LoLLauncher.exe\" \"C:/Riot Games/League of Legends/RADS/projects/lol_air_client/releases/0.0.1.0/deploy/LolClient.exe\" \"spectator {$info['spectate']['observerServerIp']}:{$info['spectate']['observerServerPort']} {$info['spectate']['observerEncryptionKey']} {$info['spectate']['gameId']} {$platform}\"";
            return $info;
        } else {
            return "Summoner doesn't exist or isn't in a spectatable game.";
        }
    }
}

?>
