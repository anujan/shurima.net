<?php

class Utilities
{

    public static $video_path = "http://d28xe8vt774jo5.cloudfront.net/abilities/videos/";
    public static $video_stills_path = "http://d28xe8vt774jo5.cloudfront.net/abilities/images/";
    public static $ability_assets_path = "/assets/images/championScreens/abilities/";
    public static $champion_assets_path = "/assets/images/champions/";
    public static $item_assets_path = "/assets/images/items/";

    public static function getChampionList()
    {
        $cache_name = 'championlist';
        if (strpos(URL::full(), "pbe.")) {
            $cache_name = "pbe_" . $cache_name;
        }
        if (!Cache::has($cache_name)) {
            $champions = Champion::order_by('name', 'asc')->get();
            $html = '';
            foreach ($champions as $champion) {
                $portrait = $champion->iconpath;
                $html .= "<a rel=\"gallery\" title=\"{$champion->displayname}\" href=\"/champions/" . Str::lower($champion->name) . "\" class=\"gallery-item cat-" . ord($champion->displayname) . "\" style=\"margin-right:0%; width:15%;\">
										  <img alt=\"{$champion->displayname}\" src=\"" . asset(static::$champion_assets_path . $portrait) . "\"><h4>{$champion->displayname}</h4></a>";
            }
            Cache::put($cache_name, $html, 60);
        }
        return Cache::get($cache_name);
    }

    public static function getItemList()
    {
        $cache_name = 'itemlist';
        if (strpos(URL::full(), "pbe.")) {
            $cache_name = "pbe_" . $cache_name;
        }
        if (!Cache::has($cache_name)) {
            $items = Item::order_by('name', 'asc')->get();
            $html = '';
            foreach ($items as $item) {
                if (!str_contains($item->description, 'Enchants')) {
                    $portrait = $item->iconpath;
                    $name = Str::slug(Str::lower(str_replace("'s", "", $item->name)));
                    $html .= "<a rel=\"gallery\" title=\"{$item->name}\" href=\"/items/" . $name . "\" class=\"gallery-item cat-" . ord($item->name) . "\" style=\"margin-right:0%; margin-bottom:0px; width:15%; height:150px;\">
										  <img alt=\"{$item->name}\" class=\"lol-icon-large\" src=\"" . asset(static::$item_assets_path . $portrait) . "\"><h4>$item->name</h4></a>";
                }
            }
            Cache::forever($cache_name, $html);
        }
        return Cache::get($cache_name);
    }

    public static function getChampions()
    {
        if (Cache::has("champions")) {
            return Cache::get("champions");
        } else {
            $champions = Champion::order_by('displayname', 'asc')->get();
            $a = array();
            foreach ($champions as $champion) {
                $a[$champion->name] = $champion->displayname;
            }
            Cache::forever("champions", $a);
            return $a;
        }
    }

    public static function getItems()
    {
        if (Cache::has("items")) {
            return Cache::get("items");
        } else {
            $items = Item::order_by('name', 'asc')->get();
            $a = array();
            foreach ($items as $item) {
                $a["{$item->id}"] = $item->name;
            }
            Cache::forever("items", $a);
            return $a;
        }
    }

    public static function adsense($type = 'horizontal')
    {
        if ($type == 'vertical') {
            $size = 'google_ad_slot = "8725232611";
            google_ad_width = 120;
            google_ad_height = 600;';
        } else {
            $size = 'google_ad_slot = "9137361811";
            google_ad_width = 728;
            google_ad_height = 90;';
        }
        return '<script type="text/javascript"><!--
            google_ad_client = "ca-pub-8742083424682828";
            ' . $size .
            '</script>
            <script type="text/javascript"
            src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
            </script>';
    }

    public function isMobileUser()
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4));
    }

    public static function getLiveStreams($limit = 50)
    {
        if (!Cache::has('streams' . $limit)) {
            $streams = Stream::where('status', '>', '0')->where('champion', '>', '0')->order_by('featured', 'desc')->order_by('viewers', 'desc')->take($limit)->get();
            $streams2 = Stream::where('status', '>', '0')->where('champion', '<', '0')->order_by('featured', 'desc')->order_by('viewers', 'desc')->take($limit - count($streams) < 0 ? 0 : $limit - count($streams))->get();
            $streams = array_merge($streams, $streams2);
            return $streams;
        }
        return Cache::get('streams' . $limit);
    }

    public static function getChatEmbed($link)
    {
        $id = static::getStreamID($link);
        if (stripos($link, 'own3d.tv')) {
            return "<iframe height=\"400\" width=\"800\" src=\"http://www.own3d.tv/chatembed/{$id}\"></iframe>";
        } else {
            return "<iframe frameborder=\"0\" scrolling=\"no\" id=\"chat_embed\" src=\"http://twitch.tv/chat/embed?channel={$id}&amp;popout_chat=true\" height=\"400\" width=\"800\"></iframe>";
        }
    }

    public static function getStreamID($url)
    {
        $url = str_replace("/live", "", $url);
        $pos = stripos($url, ".tv/") + 4;
        $str = substr($url, $pos);
        return str_replace("/", "", $str);
    }

    public static function zipFilesAndDownload($champion, $mode, $items)
    {
        $archive_file_name = $champion . ".zip";
        $zip = new ZipArchive();
        if ($zip->open($archive_file_name, ZIPARCHIVE::CREATE) !== TRUE) {
            exit("cannot open <$archive_file_name>\n");
        }
        $contents = "[ItemSet1]\r\nSetName=Set1\r\n";
        $i = 1;
        foreach ($items as $item) {
            $contents .= "RecItem$i={$item}\r\n";
            $i++;
        }
        $contents .= ";Recommended Items for " . $champion . " created using Shurima Recommended Item Changer at Shurima.net";
        $zip->addFromString("DATA/Characters/" . $champion . "/RecItems" . Str::upper($mode) . ".json", $contents);
        $zip->addFromString("README.txt", "Shurima Recommended Item Changer at Shurima.net\r\n\r\nExtract the contents of this zip file into the following folder\r\nLeague of Legends\\RADS\\solutions\\lol_game_client_sln\\releases\\0.0.0.200\\deploy\\\r\n*Replace the 0.0.0.200 with the latest client version\r\nIt should end up looking like this " . 'League of Legends\RADS\solutions\lol_game_client_sln\release s\0.0.0.200\deploy\DATA\Characters' . "\\" . $champion);
        $zip->close();
        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=$archive_file_name");
        header("Content-length: " . filesize($archive_file_name));
        header("Pragma: no-cache");
        header("Expires: 0");
        readfile("$archive_file_name");
        exit;
    }

    public static function recommendedItems()
    {
        $champion = Input::get('champion');
        $rec = array(
            'champion' => $champion,
            'title' => 'default',
            'type' => 'shurima',
            'map' => Input::get('map'),
            'priority' => true
        );
        $mode = 'any';
        switch ($rec['map']) {
            case "1":
            case "10":
                $mode = "CLASSIC";
                break;
            case "8":
                $mode = "ODIN";
                break;
            case "3":
                $mode = "ARAM";
                break;
            default:
                $mode = "any";
                break;
        }
        $rec['mode'] = $mode;
        $itemList = static::getItems();
        $starting = array('type' => 'starting');
        foreach (Input::get('starting') as $id) {
            $starting['items'][] = array('id' => $id, 'count' => 1, '_name' => $itemList["{$id}"]);
        }
        $essential = array('type' => 'essential');
        foreach (Input::get('essential') as $id) {
            $essential['items'][] = array('id' => $id, 'count' => 1, '_name' => $itemList["{$id}"]);
        }
        $offensive = array('type' => 'offensive');
        foreach (Input::get('offensive') as $id) {
            $offensive['items'][] = array('id' => $id, 'count' => 1, '_name' => $itemList["{$id}"]);
        }
        $defensive = array('type' => 'defensive');
        foreach (Input::get('defensive') as $id) {
            $defensive['items'][] = array('id' => $id, 'count' => 1, '_name' => $itemList["{$id}"]);
        }
        $rec['blocks'][] = $starting;
        $rec['blocks'][] = $essential;
        $rec['blocks'][] = $offensive;
        $rec['blocks'][] = $defensive;
        $file = tempnam("tmp", "zip");
        $zip = new ZipArchive();
        $zip->open($file, ZipArchive::OVERWRITE);
        $zip->addFromString('Config/Champions/' . $champion . '/Recommended/shurima.json', json_encode($rec));
        $zip->addFromString("README.txt", "Shurima Recommended Item Changer at Shurima.net\r\n\r\nExtract the contents into your League of Legends installation\r\n\By default this is C:\\Riot Games\\League of Legends\\");
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-Length: ' . filesize($file));
        header("Content-Disposition: attachment; filename=\"{$champion}.zip\"");
        readfile($file);
        unlink($file);
        exit;
    }

    public static function readURL($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public static function getGameHTML($game, $buttons = true)
    {
        if ($game->deleted)
            return '';
        $loadedChamps = array();
        $champion = DB::connection('sqlite')->table('champions')->find($game->champion_id);
        $loadedChamps[$game->champion_id] = $champion;
        $spoiler_id = 'spoiler_' . $game->id;
        $show_id = 'show_' . $game->id;
        $spoiler_content = '';
        $fellowPlayers = unserialize($game->fellowplayers);
        if ($fellowPlayers !== false) {
            $team_one = array();
            $team_two = array();
            foreach ($fellowPlayers as $players) {
                $cId = $players['championId'];
                if (is_array($players['championId'])) {
                    $cId = $cId['value'];
                }
                if ($players['teamId'] === 100) {
                    $team_one[] = $cId;
                } else {
                    $team_two[] = $cId;
                }
            }
            if (count($team_one) === 4) {
                $team_one[] = $game->champion_id;
            } else {
                $team_two[] = $game->champion_id;
            }
            foreach ($team_one as $t) {
                if (isset($loadedChamps[$t])) {
                    $t = $loadedChamps[$t];
                } else {
                    $t = DB::connection('sqlite')->table('champions')->find($t);
                    $loadedChamps[$t->id] = $t;
                }
                $nz = Str::lower($t->name);
                $spoiler_content .= "<a href=\"http://shurima.net/champions/{$nz}\" alt=\"{$t->displayname}\"><img width=\"50px\" height=\"50px\" src=\"" . asset(static::$champion_assets_path . $t->iconpath) . "\"></a>";
            }
            $spoiler_content .= '<p style="text-align:center; margin-bottom:5px; margin-top:5px;">VS</p>';
            foreach ($team_two as $t) {
                if (isset($loadedChamps[$t])) {
                    $t = $loadedChamps[$t];
                } else {
                    $t = DB::connection('sqlite')->table('champions')->find($t);
                    $loadedChamps[$t->id] = $t;
                }
                $nz = Str::lower($t->name);
                $spoiler_content .= "<a href=\"http://shurima.net/champions/{$nz}\" alt=\"{$t->displayname}\"><img width=\"50px\" height=\"50px\" src=\"" . asset(static::$champion_assets_path . $t->iconpath) . "\"></a>";
            }
            $teamsHTML = "<a id=\"$show_id\" onclick=\"document.getElementById('$spoiler_id').style.display=''; document.getElementById('$show_id').style.display='none';\" class=\"link\">[Show Teams]</a><span id=\"$spoiler_id\" style=\"display: none\"><a onclick=\"document.getElementById('$spoiler_id').style.display='none'; document.getElementById('$show_id').style.display='';\" class=\"link\">[Hide Teams]</a><br>$spoiler_content</span>";
        } else {
            $teamsHTML = '';
        }
        $champImg = asset(static::$champion_assets_path . $champion->iconpath);
        $minions = $game->minions_killed + $game->neutral_minions_killed;
        $link = URL::to_action('elojournal@notes', array($game->id));
        $duo = $game->premadesize > 1 ? 'Yes' : 'No';
        $date = date('M jS, Y H:i', $game->date);
        $timeStr = '';
        if ($game->ipearned != 0) {
            $timeStr = '<p class="condense"><strong>Game time:</strong> ~' . static::calculateGameTime($game) . ' mins</p>';
        }
        if (!$game->win) {
            $result = "<h4 style=\"color:#C11B17\"><strong>DEFEAT</strong></h4>";
        } else {
            $result = "<h4 style=\"color:#54BA54\"><strong>VICTORY</strong></h4>";
        }
        $ret = "<div class=\"span7\"><a href=\"http://shurima.net/champions/{$champion->name}\"><img alt=\"{$champion->displayname}\" title=\"{$champion->displayname}\" width=\"75px\" height=\"75px\" src=\"{$champImg}\"></a><div style=\"float: right; width: 55%;\"><h3>Stats</h3><p class=\"condense\">
                        <strong>Score</strong>: {$game->champions_killed} kills / {$game->num_deaths} deaths / {$game->assists} assists / {$minions} minions killed
                        </p>$timeStr
                        <p class=\"condense\">
                        <strong>Gold Earned:</strong> {$game->gold_earned}
                        </p>
                        <p class=\"condense\">
                        <strong>Ping:</strong> {$game->ping}
                        </p><p class=\"condense\">
                        <strong>Duo Queue:</strong> {$duo}
                        </p>
                        <h3 style=\"margin-top:5px\">Spells:&nbsp;<img width=\"40px\" height=\"40px\" src=\"" . asset('assets') . "/images/championScreens/spellButton{$game->spell1}.png\"><img width=\"40px\" height=\"40px\" src=\"" . asset('assets') . "/images/championScreens/spellButton{$game->spell2}.png\"></h3>
                        <h3 style=\"margin-top:5px\">Items:&nbsp;";
        for ($i = 0; $i < 6; $i++) {
            $str = 'item' . $i;
            if ($game->{$str} > 0) {
                $item = DB::connection('sqlite')->table('items')->find($game->{$str});
                if ($item === null)
                    continue;
                $item_name = Str::slug(Str::lower(str_replace("'s", "", $item->name)));
                $item_link = route('item', array($item_name));
                $ret .= "<a href=\"{$item_link}\" alt=\"{$item->name}\"><img width=\"50px\" height=\"50px\" src=\"" . asset(static::$item_assets_path . $item->iconpath) . "\"></a>";
            }
        }
        $ret .= "</h3>
                        </div>
                        <br>
                        {$result}
                        <br>
                        <h4>{$date}</h4>
                                                <br>
                        {$spoiler_content}
                        </div>
                        <div class=\"span2\"><h3>Note</h3><p class=\"condense\">{$game->note}</p>";
        if ($buttons) {
            $ret .= "<a href=\"{$link}\" style=\"color:#FFF\" class=\"btn btn-primary\">Modify Note</a>&nbsp;<button class=\"btn btn-danger\" onclick=\"rem({$game->id})\">Delete Game</button>";
        }
        $ret .= "</div>";
        return $ret;
    }

    public static function calculateGameTime($game)
    {
        $ip = $game->ipearned - $game->boostipearned;
        if ($game->win && $game->firstwin == 1) {
            $ip -= 150;
        }
        $base = $game->win ? 18 : 16;
        $perMin = $game->win ? 2.312 : 1.405;
        $time = ($ip - $base) / $perMin;
        return intval($time);
    }

    public static function alphaID($in, $to_num = false, $pad_up = false, $passKey = null)
    {
        $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if ($passKey !== null) {
            // Although this function's purpose is to just make the
            // ID short - and not so much secure,
            // with this patch by Simon Franz (http://blog.snaky.org/)
            // you can optionally supply a password to make it harder
            // to calculate the corresponding numeric ID

            for ($n = 0; $n < strlen($index); $n++) {
                $i[] = substr($index, $n, 1);
            }

            $passhash = hash('sha256', $passKey);
            $passhash = (strlen($passhash) < strlen($index)) ? hash('sha512', $passKey) : $passhash;

            for ($n = 0; $n < strlen($index); $n++) {
                $p[] = substr($passhash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            $index = implode($i);
        }

        $base = strlen($index);

        if ($to_num) {
            // Digital number  <<--  alphabet letter code
            $in = strrev($in);
            $out = 0;
            $len = strlen($in) - 1;
            for ($t = 0; $t <= $len; $t++) {
                $bcpow = bcpow($base, $len - $t);
                $out = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
            }

            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $out -= pow($base, $pad_up);
                }
            }
            $out = sprintf('%F', $out);
            $out = substr($out, 0, strpos($out, '.'));
        } else {
            // Digital number  -->>  alphabet letter code
            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $in += pow($base, $pad_up);
                }
            }

            $out = "";
            for ($t = floor(log($in, $base)); $t >= 0; $t--) {
                $bcp = bcpow($base, $t);
                $a = floor($in / $bcp) % $base;
                $out = $out . substr($index, $a, 1);
                $in = $in - ($a * $bcp);
            }
            $out = strrev($out); // reverse
        }

        return $out;
    }

    public static function makeShurimaAPIRequest($region, $method, $args)
    {
        $PORT = 714;
        $HOST = "78.129.218.131";
        $args = urldecode($args);
        $sock = socket_create(AF_INET, SOCK_STREAM, 0);
        $succ = socket_connect($sock, $HOST, $PORT);
        $text = $region . "&" . $method . "&" . $args;
        socket_write($sock, $text . "\n", strlen($text) + 1);
        $reply = socket_read($sock, 10000000, PHP_NORMAL_READ);
        return $reply;
    }

    public static function updateStreams()
    {
        if (Cache::has('updated_streams')) {
            return;
        } else {
            Cache::put('updated_streams', 4, 4);
            try {
                    $streams = Stream::get();
                    $stream_string = '';
                    foreach ($streams as $stream) {
                        if (strlen($stream_string) > 1) {
                            $stream_string .= ',';
                        }
                        $stream_string .= $stream->channel;
                    }
                    $twitch = Utilities::readURL("https://api.twitch.tv/kraken/streams?channel={$stream_string}");
                    $json = json_decode($twitch, true);
                    DB::connection('shurima2')->table('streams')->update(array('status' => 0, 'viewers' => 0));
                    if (!isset($json['streams'])) {
                        Log::info(var_export($json, true));
                        exit(1);
                    }
                    unset($streams);
                    if ($json['streams'] != null) {
                        $stream_array = $json['streams'];
                        foreach ($stream_array as $stream) {
                            $s = Stream::where('channel', '=', $stream['channel']['name'])->first();
                            $s->viewers = intval($stream['viewers']);
                            if ($s->status == 0 || $s->start_time == 0) {
                                $s->start_time = time();
                            }
                            $s->status = 1;
                            if (is_array($stream['preview'])) {
                                $stream['preview'] = $stream['preview']['medium'];
                            }
                            $s->thumbnail = $stream['preview'];
                            $s->title = $stream['channel']['status'];
                            $s->save();
                        }
                    }
                    $streams = Stream::where('status', '>', '0')->order_by('viewers', 'desc')->get();
                    foreach ($streams as $s) {
                        $spectate = Spectate::find($s->ign, $s->region);
                        if (!is_array($spectate)) {
                            $s->champion = -1;
                            $s->matchup = '';
                        } else {
                            if ($s->matchup === serialize(($spectate)))
                                continue;
                            $s->matchup = serialize($spectate);
                            $found = false;
                            $teamKeys = array("teamone", "teamtwo");
                            for ($i = 0; $i < 2 && !$found; $i++) {
                                foreach ($spectate[$teamKeys[$i]] as $key => $val) {
                                    $percent;
                                    if (isset($val['name'])) {
                                        similar_text($val['name'], $s->ign, $percent);
                                    }
                                    if (isset($val['summonerId']) && ($val['summonerId'] == $s->summonerid || $val['accountId'] == $s->acctid || $percent > 70)) {
                                        $s->champion = $val['champion'];
                                        $s->acctId = $val['accountId'];
                                        $s->runepage = serialize(Utilities::getCurrentRunepage($s->region, $s->acctId));
                                        $s->masterypage = serialize(Utilities::getCurrentMasteryPage($s->region, $s->summonerid));
                                        VideoArchive::addGame($s);
                                        $found = true;
                                    }
                                }
                            }
                        }
                        $s->save();
                    }
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
        }
    }

    public static function updateLeagues()
    {
        if (Cache::has('update_leagues')) {
            return;
        }
        Cache::put('update_leagues', 30, 30);
        $users = Stream::get();
        foreach ($users as $streamer) {
            $data = static::makeShurimaAPIRequest($streamer->region, 'leagues', $streamer->summonerid);
            if ($streamer->region === 'kr')
                continue; //no support for korea :/
            $json = json_decode($data, true);
            if (!is_array($json))
                continue;
            foreach ($json as $league) {
                if ($league['queue'] === 'RANKED_SOLO_5x5') {
                    $streamer->tier = $league['tier'];
                    $streamer->division = $league['division'];
                    $streamer->save();
                    break;
                }
            }
        }
        Cache::put('update_leagues', 30, 30);
    }

    public static function getCurrentMasteryPage($region, $summonerId)
    {
        $data = json_decode(static::makeShurimaAPIRequest($region, 'mastery_pages', $summonerId), true);
        foreach ($data['array'] as $page) {
            if ($page['current']) {
                $masteries = array();
                $classes = array();
                $names = array();
                for ($i = 500; $i <= 700; $i += 100) {
                    for ($j = 0; $j <= 60; $j += 10) {
                        for ($k = 0; $k <= 5; $k++) {
                            $masteries[$i + $j + $k] = 0;
                            $classes[$i + $j + $k] = 'no_rank';
                            $names[$i + $j + $k] = '';
                        }
                    }
                }
                foreach ($page['talentEntries']['array'] as $talent) {
                    $tId = $talent['talentId'];
                    $rank = $talent['rank'];
                    $masteries[$tId] = $rank;
                    $masteries[($tId - ($tId % 100))] += $rank;
                    if ($rank == $talent['talent']['maxRank']) {
                        $classes[$tId] = 'max_rank';
                    } else if ($rank > 0) {
                        $classes[$tId] = '';
                    }
                    $names[$tId] = $talent['talent']['name'];
                }
                $masteries['name'] = $page['name'];
                $masteries['names'] = $names;
                $masteries['classes'] = $classes;
                return $masteries;
            }
        }
    }

    public static function getAcctIdByName($name, $region)
    {
        $json = Utilities::makeShurimaAPIRequest($region, 'summoner', $name);
        $json = json_decode($json, true);
        return intval($json['acctId']);
    }

    public static function getCurrentRunepage($region, $acctId)
    {
        $data = json_decode(static::makeShurimaAPIRequest($region, 'rune_pages', $acctId), true);
        foreach ($data as $runepage) {
            if ($runepage['page']['current']) {
                usort($runepage['page']['slotEntries']['array'], function ($a, $b) {
                    if ($a['runeSlotId'] == $b['runeSlotId']) {
                        return 0;
                    }
                    return ($a['runeSlotId'] < $b['runeSlotId']) ? -1 : 1;
                });
                $runepage['statistics'] = array();
                for ($i = 0; $i < count($runepage['page']['slotEntries']['array']); $i++) {
                    $desc = $runepage['page']['slotEntries']['array'][$i]['rune']['description'];
                    if (count($runepage['page']['slotEntries']['array'][$i]['rune']['itemEffects']['array'] == 2)) {
                        $desc = explode(" / ", $desc);
                    }
                    if (!is_array($desc)) {
                        $desc = array();
                        $desc[0] = $desc;
                    }
                    foreach ($desc as $d) {
                        $space = strpos($d, " ");
                        $val = (float)$d;
                        $key = trim(substr($d, $space));
                        if ($pos = strpos($key, "(+")) {
                            $key = trim(substr($key, 0, $pos - 1));
                        }
                        if (!isset($runepage['statistics'][$key])) {
                            $runepage['statistics'][$key] = 0;
                        }
                        $runepage['statistics'][$key] += $val;
                    }
                }
                return $runepage;
            }
        }
        return $data;
    }

    public static function getRuneImageString($rune)
    {
        if ($rune['itemId'] >= 8000) {
            return $rune['itemId'];
        }
        $ret = '';
        switch ($rune['runeType']['name']) {
            case 'Black':
                $ret = 'bl_';
                break;
            case 'Red':
                $ret = 'r_';
                break;
            case 'Blue':
                $ret = 'b_';
                break;
            case 'Yellow':
                $ret = 'y_';
                break;
        }
        $ret .= ($rune['runeType']['runeTypeId'] > 3 ? 3 : $rune['runeType']['runeTypeId']);
        $ret .= '_' . $rune['tier'];
        return $ret;
    }

    public static function getEloJournalStats()
    {
        if (Cache::has('elostats')) {
            return Cache::get('elostats');
        }
        $elojournal = array();
        $elojournal['users'] = User::count();
        $elojournal['games'] = Game::count();
        $elojournal['wins'] = Game::where('win', '>', 0)->count();
        $elojournal['losses'] = $elojournal['games'] - $elojournal['wins'];
        $elojournal['winrate'] = (float)$elojournal['wins'] / (float)$elojournal['games'];
        $mostPlayedChampion = Game::group_by('champion_id')->order_by('cnt', 'desc')->first(array('champion_id', DB::raw('COUNT(*) as cnt')));
        $elojournal['mostplayedchampion'] = DB::connection('sqlite')->table('champions')->find($mostPlayedChampion->champion_id)->displayname . " (" . $mostPlayedChampion->cnt . " Games)";
        $highestChampWinrate = Game::group_by('champion_id')->order_by('cnt', 'desc')->first(array('champion_id', DB::raw('AVG(win) as cnt')));
        $elojournal['highestwinrate'] = DB::connection('sqlite')->table('champions')->find($highestChampWinrate->champion_id)->displayname . " (" . ($highestChampWinrate->cnt * 100) . "%)";
        $lowestWinRate = Game::group_by('champion_id')->order_by('cnt', 'asc')->first(array('champion_id', DB::raw('AVG(win) as cnt')));
        $elojournal['lowestwinrate'] = DB::connection('sqlite')->table('champions')->find($lowestWinRate->champion_id)->displayname . " (" . ($lowestWinRate->cnt * 100) . "%)";
        Cache::put('elostats', $elojournal, 5);
        return $elojournal;
    }
}

?>
