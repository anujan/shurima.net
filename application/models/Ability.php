<?php
class Ability extends Eloquent {
    public static $timestamp = false;
    public static $table = 'championAbilities';
    
    public function champion() {
        return $this->belongs_to('Champion', 'championid');
    }
}
?>
