<?php

class User extends Eloquent {

    public static $connection = 'shurima';

    public function games() {
        return $this->has_many('Game', 'user_id');
    }

    public function get_gravatar( $s = 130, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $this->email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    public function updateGames() {
        if ($this->verify !== '')
            return 1;
        $gameArray = array();
        if ($this->last_update + 1800 < time() && $this->verify === '') {
            if (Cache::has($this->id . "_champwithwinrate")) {
                Cache::forget($this->id . "_champwithwinrate");
            }
            if ($this->server === 'br') { //Use Elophant
                $region = Str::lower($this->server);
                $url = "http://api.elophant.com/v2/$region/recent_games/{$this->acctid}?key=ePOaLGGLhF5WHOdHFhF3";
                $json = Utilities::readURL($url);
                if ($json === false)
                    return null;
                $data = json_decode($json, true);
                if (isset($data['error'])) {
                    echo $data['error'];
                    exit(1);
                }
                $games = $data['data']['gameStatistics'];
                $new_time = $this->last_game_time;
                if (!is_array($games)) {
                    $errmsg = var_export($games);
                    Log::error("GameStats array using EloJournal for {$this->username}, summoner: {$this->summoner} on {$this->server}/r/n{$errmsg}");
                    return;
                }
                foreach ($games as $game) {
                    if ($game['ranked'] === true) {
                        //$t = strtotime($game['createDate']);
                        preg_match('/([\d]{10})/', $game['createDate'], $matches);
                        $t = $matches[0];
                        if ($t <= $this->last_game_time)
                            continue;
                        $g = new Game;
                        $g->user_id = $this->id;
                        $g->spell1 = $game['spell1'];
                        $g->spell2 = $game['spell2'];
                        $g->date = $t;
                        if ($t > $new_time)
                            $new_time = $t;
                        $g->ping = $game['userServerPing'];
                        $g->rating = 0;
                        $g->premadesize = $game['premadeSize'];
                        $g->fellowPlayers = serialize($game['fellowPlayers']);
                        $g->game_id = $game['gameId'];
                        $g->champion_id = $game['championId'];
                        $g->ipearned = $game['ipEarned'];
                        $g->boostipearned = $game['boostIpEarned'];
                        $g->firstwin = $game['eligibleFirstWinOfDay'] ? 1 : 0;
                        $stats = $game['statistics'];
                        foreach ($stats as $stat) {
                            $s = Str::lower($stat['statType']);
                            if ($s === 'lose') {
                                $g->win = 0;
                            } else
                                $g->{$s} = $stat['value'];
                        }
                        $g->save();
                        $gameArray[$t] = $g;
                    }
                }
                $this->last_game_time = $new_time;
                $this->last_update = time();
                $this->save();
            } else {
                $region = Str::lower($this->server);
                $method = 'recent_games';
                $args = $this->acctid;
                $json = Utilities::makeShurimaAPIRequest($region, $method, $args);
                $data = json_decode($json, true);
                $games = $data['array'];
                if (!is_array($games)) {
                    return null;
                }
                $new_time = $this->last_game_time;
                foreach ($games as $game) {
                    $g = $game;
                    if ($g['ranked'] !== true)
                        continue;
                    $t = strtotime($g['createDate']);
                    if ($t <= $this->last_game_time)
                        continue;
                    $game = new Game;
                    $game->user_id = $this->id;
                    $game->spell1 = $g['spell1'];
                    $game->spell2 = $g['spell2'];
                    $game->date = $t;
                    if ($t > $new_time)
                        $new_time = $t;
                    $game->ping = $g['userServerPing'];
                    $game->rating = $g['adjustedRating'];
                    $game->premadesize = $g['premadeSize'];
                    $game->game_id = $g['gameId'];
                    $game->ipearned = $g['ipEarned'];
                    $game->boostipearned = $g['boostIpEarned'];
                    $game->firstwin = $g['eligibleFirstWinOfDay'] ? 1 : 0;
                    $fellowPlayers = array();
                    $fellow = $g['fellowPlayers']['array'];
                    foreach ($fellow as $f) {
                        $championId = intval($f['championId']);
                        $summonerId = intval($f['summonerId']);
                        $teamId = intval($f['teamId']);
                        $fellowPlayers[] = array('championId' => $championId, 'summonerId' => $summonerId, 'teamId' => $teamId);
                    }
                    $game->fellowPlayers = serialize($fellowPlayers);
                    $game->champion_id = $g['championId'];
                    $stats = $g['statistics']['array'];
                    foreach ($stats as $stat) {
                        $s = Str::lower($stat['statType']);
                        if ($s === 'lose') {
                            $game->win = 0;
                        } else
                            $game->{$s} = intval($stat['value']);
                    }
                    $game->save();
                    $gameArray[$t] = $game;
                }
                $this->last_game_time = $new_time;
                $this->last_update = time();
                $this->save();
            }
        }
        krsort($gameArray);
        return $gameArray;
    }

    public function verify() {
        $region = Str::lower($this->server) === 'na' ? '' : Str::lower($this->server);
        if ($region == 'br') { //Use Elophant instead
            if ($region == '')
                $region = 'na';
            $sum = urlencode($this->summoner);
            $json = Utilities::readURL("http://api.elophant.com/v2/{$region}/summoner/{$sum}?key=ePOaLGGLhF5WHOdHFhF3");
            $data = json_decode($json, true);
            if (!isset($data['data'])) {
                echo '<meta http-equiv="REFRESH" content="1;url=' . action('elojournal@settings') . '"></HEAD>';
                echo "There is no " . $this->summoner . " on " . Str::upper($region);
                echo ". Please check your settings to see if you have the correct information!";
                exit(1);
            }
            $data = $data['data'];
            $summonerId = $data['summonerId'];
            $this->acctid = $data['acctId'];
            $json = Utilities::readURL("http://api.elophant.com/v2/{$region}/rune_pages/{$summonerId}?key=ePOaLGGLhF5WHOdHFhF3");
            $data = json_decode($json, true);
            if ($data['success'] === false) {
                Log::error($data['error']);
                echo '<meta http-equiv="REFRESH" content="1;url=' . action('elojournal@settings') . '"></HEAD>';
                echo $data['error'];
                echo ". Please check your settings to see if you have the correct information!";
                exit(1);
            }
            $data = $data['data'];
            $bookPages = $data['bookPages'];
            /* if (!is_array($bookPages)) {
              echo '<meta http-equiv="REFRESH" content="1;url=' . action('elojournal@settings') . '"></HEAD>';
              echo "There is no " . $this->summoner . " on " . Str::upper($region);
              echo ". Please check your settings to see if you have the correct information!";
              exit(1);
              } */
            foreach ($bookPages as $page) {
                if ($this->verify === $page['name']) {
                    $this->verify = '';
                    $this->save();
                    return true;
                }
            }
        } else {
            if ($this->acctid == 0) {
                $region = Str::lower($this->server);
                $sum = $this->summoner;
                $method = 'summoner';
                $json = Utilities::makeShurimaAPIRequest($region, $method, $sum);
                $json = json_decode($json, true);
                $this->acctid = intval($json['acctId']);
                if ($this->acctid === 0)
                    return false;
                else
                    $this->save();
            }
            $json = Utilities::makeShurimaAPIRequest($this->server, 'rune_pages', $this->acctid);
            $data = json_decode($json, true);
            if (is_array($data)) {
                foreach ($data as $page) {
                    if ($page['name'] == $this->verify) {
                        $this->verify = '';
                        $this->save();
                        return true;
                    }
                }
            } else {
                Log::write('info', var_export($json, true));
                return false;
            }
        }
        return false;
    }
    
    public function getShareKey() {
        return Utilities::alphaID($this->acctid);
    }

    public function getChampsWithWinRate() {
        if (Cache::has($this->id . "_champwithwinrate")) {
            return Cache::get($this->id . "_champwithwinrate");
        }
        $wins = array();
        foreach ($this->games()->get() as $game) {
            if (!isset($wins[$game->champion_id][0]))
                $wins[$game->champion_id][0] = 0;
            if (!isset($wins[$game->champion_id][1]))
                $wins[$game->champion_id][1] = 0;
            $wins[$game->champion_id][0] += $game->win;
            $wins[$game->champion_id][1]++;
            $wins[$game->champion_id][10] = $game->champion_id;
            $fellowPlayers = unserialize($game->fellowplayers);
            if ($fellowPlayers !== false) {
                $team_one = array();
                $team_two = array();
                foreach ($fellowPlayers as $players) {
                    $cId = $players['championId'];
                    if (is_array($players['championId'])) {
                        $cId = $cId['value'];
                    }
                    for ($z = 0; $z < 9; $z++) {
                        if (!isset($wins[$cId][$z])) {
                            $wins[$cId][$z] = 0;
                        }
                    }
                    if ($players['teamId'] === 100) {
                        $team_one[] = $cId;
                    } else {
                        $team_two[] = $cId;
                    }
                    if (count($team_one) === 4) { //allied team
                        if (!isset($wins[$cId][4])) {
                            $wins[$cId][4] = 1;
                            $wins[$cId][3] = 0;
                        } else {
                            $wins[$cId][4]++;
                        }
                        $wins[$cId][3] += $game->win;
                    } else { //enemy team
                        if (!isset($wins[$cId][7])) {
                            $wins[$cId][7] = 1;
                            $wins[$cId][6] = 0;
                        } else {
                            $wins[$cId][7]++;
                        }
                        $wins[$cId][6] += $game->win ? 0 : 1;
                    }
                    $wins[$cId][10] = $cId;
                }
            }
        }
        foreach ($wins as $champ) {
            for ($i = 0; $i < 9; $i++) {
                if (!isset($champ[$i])) $champ[$i] = 0;
            }
            $champ[2] = $champ[1] === 0 ? 0 : $champ[0] / $champ[1];
            $champ[5] = $champ[4] === 0 ? 0 : $champ[3] / $champ[4];
            $champ[8] = $champ[7] === 0 ? 0 : $champ[6] / $champ[7];
        }
        Cache::put($this->id . "_champwithwinrate", $wins, 60 * 24 * 30);
        return $wins;
    }

    /**
     * Establish the relationship between a user and posts
     *
     * @return Laravel\Database\Eloquent\Relationships\Has_Many
     */
    public function posts()
    {
        return $this->has_many('Post');
    }

    /**
     * Establish the relationship between a user and comments
     *
     * @return Laravel\Database\Eloquent\Relationships\Has_Many
     */
    public function comments()
    {
        return $this->has_many('Comment');
    }
}

?>