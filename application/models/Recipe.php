<?php
class Recipe extends Eloquent {
    
    public static $table = 'itemRecipes';
    public static $timestamp = false;
    
    public function itemFrom() {
        return $this->belongs_to('Item', 'recipeitemid');
    }
    
    public function itemTo() {
        return $this->belongs_to('Item', 'buildstoitemid');
    }
}
?>
