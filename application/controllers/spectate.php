<?php

class Spectate_Controller extends Base_Controller {

        public function action_index() {
                return View::make('spectate.index');
        }

        public function action_search() {
                $summoner = Input::get('summoner');
                $region = Input::get('region');
                $error = null;
                $validRegions = array('na', 'euw', 'eune', 'br', 'tr', 'tw', 'sg');
                if (!in_array($region, $validRegions)) {
                        $error = "That's not a valid region";
                } else {
                        $spectate = Spectate::find($summoner, $region);
                        if ($spectate === null) {
                                $error = "There seems to be something wrong with our system, try again later";
                        } else if (is_array($spectate)) {
                                return View::make('spectate.search')->with('spectate', $spectate);
                        } else {
                                $error = $spectate;
                        }
                }
                if ($error !== null) {
                        return View::make('spectate.index')->with('error', $error);
                }
        }
}

?>
