<?php

class EloJournal_Controller extends Base_Controller
{

    public function action_index()
    {
        if (Auth::check()) {
            $games = Auth::user()->games()->order_by('date', 'desc')->paginate(10);
            return View::make('elojournal.index')->with('paginate', $games);
        }
        return View::make('elojournal.index');
    }

    public function action_update()
    {
        if (Auth::check()) {
            $games = null;
            try {
                $games = Auth::user()->updateGames();
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
            if ($games === 1) {
                echo 'Please verify your account by changing one of your rune page names to "'.Auth::user()->verify.'" and clicking the button below...<br><a class="btn btn-primary" href="'.action('elojournal@verify').'">Click here to verify your account</a>';
            }
            if ($games === null) {
                echo '<h2>Our servers seem to be down at the moment, please try again later.</h2>';
                return;
            }
            if (count(Auth::user()->games()->get()) < 1) {
                echo '<h2>You have no ranked games in your match history. Start playing some so we can keep track of them for you!</h2>';
            }
            if (!is_array($games)) {
                echo '';
            } else {
                foreach ($games as $game) {
                    echo Utilities::getGameHTML($game);
                }
            }
            return;
        }
        return '';
    }

    public function action_faq()
    {
        return View::make('elojournal.faq');
    }

    public function action_register()
    {
        if (Request::method() == 'GET') {
            return View::make('elojournal.register');
        } else {
            $input = array(
                'username' => Input::get('username'),
                'password' => Input::get('password'),
                'captcha' => Input::get('captcha'),
                'password_confirmation' => Input::get('password_confirmation'),
                'summoner' => Input::get('summoner'),
                'server' => Input::get('server'),
                'email' => Input::get('email')
            );

            $rules = array(
                'username' => 'required|min:4|max:30|unique:users',
                'summoner' => 'required',
                'verify' => 'laracaptcha',
                'password' => 'required|min:6|confirmed',
                'server' => 'required',
                'email' => 'required|email|unique:users'
            );
            $v = Validator::make($input, $rules);
            if ($v->fails()) {
                $emsg = $v->errors->first();
                return View::make('elojournal.register')->with_input('only', array('username', 'email', 'server', 'summoner'))->with('register_error', $emsg);
            } else {
                $password = Hash::make(Input::get('password'));
                $user = new User;
                $user->username = Input::get('username');
                $user->email = Input::get('email');
                $user->password = $password;
                $user->summoner = Input::get('summoner');
                $user->server = Str::lower(Input::get('server'));
                $verifyCode = "";
                $allowed_symbols = '23456789abcdeghkmnpqsuvxyz';
                for ($i = 0; $i < 11; $i++) {
                    $verifyCode .= $allowed_symbols[mt_rand(0, strlen($allowed_symbols) - 1)];
                }
                $user->verify = $verifyCode;
                $user->save();
                $userdata = array(
                    'username' => Input::get('username'),
                    'password' => Input::get('password'),
                    'remember' => true
                );
                Auth::attempt($userdata);
                return Redirect::to_action('elojournal@index');
            }
        }
    }

    public function action_login()
    {
        if (Auth::check())
            return View::make('elojournal.index');
        if (Request::method() == 'GET') {
            return View::make('elojournal.login');
        } else {
            $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password'),
                'remember' => true
            );
            if (Auth::attempt($userdata)) {
                if (Input::has('redirect') && Str::length(Input::get('redirect')) > 3) {
                    return Redirect::to(Input::get('redirect'));
                }
                return Redirect::to_action('elojournal@index');
            } else {
                return View::make('elojournal.login')
                    ->with('login_errors', true)->with_input('only', array('username', 'redirect'));
            }
        }
    }

    public function action_logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }
        return Redirect::to_action('elojournal@index');
    }

    public function action_notes($id)
    {
        if (Auth::guest()) {
            return Redirect::to_action('elojournal@login');
        }
        if (Request::method() == 'GET') {
            $g = Game::find($id);
            if ($g === null || $g->user_id != Auth::user()->id) {
                return Response::error('500');
            }
            return View::make('elojournal.notes')->with('game', $g);
        } else {
            $g = null;
            if (Input::has('note')) {
                $g = Game::find($id);
                if ($g === null || $g->user_id != Auth::user()->id) {
                    return Response::error('500');
                }
                $g->note = Input::get('note');
                $g->save();
            } else {
                return Response::error('500');
            }
            return Redirect::to_action('elojournal@index');
        }
    }

    public function action_delete($id)
    {
        if (Auth::guest()) {
            return Redirect::to_action('elojournal@login');
        }
        if (Request::method() == 'GET') {
            $g = Game::find($id);
            if ($g === null || $g->user_id != Auth::user()->id) {
                return Response::error('500');
            }
            $g->deleted = 1;
            $g->save();
            return true;
        }
    }

    public function action_settings()
    {
        if (Auth::guest()) {
            return Redirect::to_action('elojournal@login');
        }
        if (Request::method() == 'GET') {
            return View::make('elojournal.settings');
        } else {
            $input = Input::all();
            $rules = array(
                'summoner' => 'required',
                'password' => 'required|min:6|alpha_num|confirmed',
                'server' => 'required',
                'email' => 'required|email|unique:users,email,' . Auth::user()->id,
                'about' => 'max:250'
            );
            $v = Validator::make($input, $rules);
            if ($v->fails()) {
                $emsg = $v->errors->first();
                return View::make('elojournal.settings')->with_input('only', array('summoner', 'email', 'server'))->with('error_message', $emsg);
            } else {
                $user = Auth::user();
                $server = Input::get('server');
                $summoner = Input::get('summoner');
                if ($server != $user->server || $summoner != $user->summoner) {
                    $verifyCode = "";
                    $allowed_symbols = '23456789abcdeghkmnpqsuvxyz';
                    for ($i = 0; $i < 11; $i++) {
                        $verifyCode .= $allowed_symbols[mt_rand(0, strlen($allowed_symbols) - 1)];
                    }
                    $user->verify = $verifyCode;
                    $user->acctid = 0;
                    $user->last_game_time = 0;
                }
                if ($user->role > 0) {
                    $user->about = Input::get('about');
                }
                $user->summoner = $summoner;
                $user->password = Hash::make(Input::get('password'));
                $user->server = Str::lower($server);
                $user->email = Input::get('email');
                $user->save();
                return Redirect::to_action('elojournal@settings');
            }
        }
    }

    public function action_view($id = null)
    {
        $u = null;
        if ($id == null) {
            $id = Auth::user()->acctid;
        } else {
            $id = Utilities::alphaID($id, true);
        }
        $u = User::where('acctid', '=', $id)->where('verify','=', '')->first();
        if ($u == null) {
            return Response::error('500');
        }
        $games = $u->games()->order_by('date', 'desc')->paginate(10);
        return View::make('elojournal.view')->with('paginate', $games);
    }

    public function action_verify()
    {
        if (Auth::guest()) {
            return Redirect::to_action('elojournal@login');
        } else if (Auth::user()->verify === '') {
            return Redirect::to_action('elojournal@index');
        } else {
            $status = Auth::user()->verify();
            if ($status === true) {
                return '<meta http-equiv="REFRESH" content="1;url=' . action('elojournal@index') . '"></HEAD>Your account has been verified. You will now be redirected back to our website!';
            } else {
                return 'Verification failed. Please change one of your rune page name to "' . Auth::user()->verify . '" and try again...<br />If you have already changed it, and are still getting this message it means are servers are down, so try again later.';
            }
        }
    }

    public function action_debug()
    {
        $paginate = Auth::user()->games()->order_by('date', 'desc')->paginate(10);
        print_r($paginate);
        exit(1);
    }

    public function action_share()
    {
        return Redirect::to_action('elojournal');
    }

    public function action_stats()
    {
        if (Auth::guest())
            return Redirect::to_action('elojournal@login');
        $user = Auth::user();
        $winrates = $user->getChampsWithWinRate();
        if (count($winrates)) {
            for ($i = 0; $i <= max(array_keys($winrates)); $i++) {
                if (!isset($winrates[$i]))
                    continue;
                $winrates[$i][10] = DB::connection('sqlite')->table('champions')->find($winrates[$i][10])->displayname;
                $winrates[$i][2] = round($winrates[$i][1] === 0 ? 0 : $winrates[$i][0] / $winrates[$i][1], 3);
                $winrates[$i][5] = round($winrates[$i][4] === 0 ? 0 : $winrates[$i][3] / $winrates[$i][4], 3);
                $winrates[$i][8] = round($winrates[$i][7] === 0 ? 0 : $winrates[$i][6] / $winrates[$i][7], 3);
            }
        }
        return View::make('elojournal.stats')->with('stats', $winrates);
    }

    public function action_forgotpassword() {
        if (Request::method() == 'GET') {
            return View::make('elojournal.forgot');
        } else {
            $u = User::where('email', '=', Input::get('email'))->or_where('username', '=', Input::get('email'))->first();
            if ($u === null){
                return View::make('elojournal.forgot')->with('error', 'The username/email provided is not associated with an account');
            }
            $verifyCode = "";
            $allowed_symbols = '23456789abcdeghkmnpqsuvxyz';
            for ($i = 0; $i < 11; $i++) {
                $verifyCode .= $allowed_symbols[mt_rand(0, strlen($allowed_symbols) - 1)];
            }
            Cache::put('forgot_'.$u->id, $verifyCode, 60 * 24);
            Bundle::start('swiftmailer');
            $mailer = IoC::resolve('mailer');
            $message = Swift_Message::newInstance('Forgot Password')
                ->setFrom(array('noreply@shurima.net' => 'Shurima Network'))
                ->setTo(array($u->email => $u->username))
                ->setBody('<a href="http://shurima.net/elojournal/resetpassword/'.$u->id.'/'.$verifyCode.'">Visit this link to reset your password</a>', 'text/html');
            $mailer->send($message);
            return View::make('elojournal.forgot')->with('success', 'Check your email for your password recovery link! It only lasts 24 hours.');
        }
    }

    public function action_resetpassword($id, $code) {
        $u = User::find($id);
        if ($u === null)
            return Response::error('500');
        if (Cache::has('forgot_'.$u->id)){
            if (Cache::get('forgot_'.$u->id) == $code) {
                Bundle::start('swiftmailer');
                $mailer = IoC::resolve('mailer');
                $message = Swift_Message::newInstance('Forgot Password')
                    ->setFrom(array('noreply@shurima.net' => 'Shurima Network'))
                    ->setTo(array($u->email => $u->username))
                    ->setBody('Your password has been changed to <b>'.$code.'</br>. Login to your account and change it through the settings page.', 'text/html');
                $mailer->send($message);
                $u->password = $code;
                $u->save();
                return View::make('elojournal.forgot')->with('success', 'Check your email for your new password');
            }
        }
        return View::make('elojournal.forgot')->with('error', 'Your password recovery attempt expired. Please try again.');
    }
}

?>