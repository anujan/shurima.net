<?php

class Blog_Controller extends Base_Controller {

    public $restful = true;
    /**
     * Display all of the blog posts
     *
     * @return void
     */
    public function get_index()
    {
        $posts = Post::order_by('id', 'desc')->paginate(10);

        return View::make('blog.index')->with('posts', $posts);
    }


    public function get_posts()
    {
        $posts = Post::order_by('id', 'desc')->paginate(10);

        return View::make('blog.index')->with('posts', $posts);
    }

    /**
     * Display a specific blog post
     *
     * @param int $id
     * @return void
     */
    public function get_post($id)
    {
        $post = Post::find($id);
        if(is_null($post))
        {
            return Redirect::home();
        }
        return View::make('blog.post')->with('post', $post);
    }

    /**
     * Add a comment to a specific blog post
     *
     * @param int $post_id
     * @return Redirect
     */
    public function post_post($post_id)
    {
        $post = Post::find($post_id);
        if (Auth::guest()) {
            return Redirect::to_action('elojournal@login');
        }
        if(is_null($post))
        {
            return Redirect::home();
        }

        $validation = Validator::make(Input::all(), array(
            'comment' => 'required',
        ));

        if($validation->fails())
        {
            return Redirect::to('blog/post/' . $post_id)->with_errors($validation->errors);
        }

        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $post_id;
        $comment->content = Input::get('comment');

        $comment->save();

        return Redirect::to('blog/post/' . $post_id);
    }

    public function get_add() {
        if (Auth::check() && Auth::user()->role > 0) {
            return View::make('blog.add');
        }
        return Response::error('404');
    }

    public function post_add() {
        if (Auth::user()->role > 0) {
            $post = new Post;
            $post->user_id = Auth::user()->id;
            $post->content = Input::get('content');
            $post->title = Input::get('title');
            $post->save();
            return Redirect::to('blog/post/' . $post->id);
        }
        return Response::error('404');
    }

    public function get_edit($id) {
        if (Auth::check() && Auth::user()->role > 0) {
            $post = Post::find($id);
            if (Auth::user()->role > 1 || ($post !== null && Auth::user()->id === $post->user_id)) {
                return View::make('blog.edit')->with('post', $post);
            }
        }
        return Response::error('404');
    }

    public function post_edit($id) {
        if (Auth::check() && Auth::user()->role > 0) {
            $post = Post::find($id);
            if (Auth::user()->role > 1 || ($post !== null && Auth::user()->id === $post->user_id)) {
                $post->content = Input::get('content');
                $post->title = Input::get('title');
                $post->save();
                return Redirect::to('blog/post/' . $post->id);
            }
        }
        return Response::error('500');
    }
}
?>