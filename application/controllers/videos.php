<?php

class Videos_Controller extends Base_Controller {

    public function action_index($id = null, $slug = '') { //List the
        if ($id === null) {
            $paginate = Video::order_by('id', 'desc')->where('pending', '=', '0')->paginate(20);
            return View::make("videos.index")->with('videos', $paginate);
        } else {
            $vod = Video::find($id);
            if ($vod === null) {
                return Response::error('404');
            } else if (empty($slug)) {
                return Redirect::to_action("videos", array($id, Str::slug($vod->ign . " playing " . Champion::find($vod->champion)->displayname)));
            }
        }
    }

    public function action_moderate() {
        if (Auth::check() && Auth::user()->role >= 2) {

        }
        return Response::error('404');
    }
}
?>